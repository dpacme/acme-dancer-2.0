package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Style;
import domain.StyleRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class StyleRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private DancerService dancerService;

	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private StyleRecordService		styleRecordService;

	@Autowired
	private StyleService			styleService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un registro profesional de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El id del curriculum no existe
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del style no existe
	 */
	public void createStyleRecord(String username, Integer curriculaId, Integer years, Integer styleId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			StyleRecord styleRecord = this.styleRecordService.create(this.curriculaService.findOne(curriculaId));
			Style style = this.styleService.findOne(styleId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(styleRecord.getCurricula()));

			styleRecord.setYears(years);
			styleRecord.setStyle(style);

			Assert.isTrue(styleRecord.getYears() != null);
			this.styleRecordService.save(styleRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un registro profesional de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del styleRecord no existe
	 * � El id del style no existe
	 */
	public void editStyleRecord(String username, Integer styleRecordId, Integer years, Integer styleId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			StyleRecord styleRecord = this.styleRecordService.findOne(styleRecordId);
			Style style = this.styleService.findOne(styleId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(styleRecord.getCurricula()));

			styleRecord.setYears(years);
			styleRecord.setStyle(style);

			Assert.isTrue(styleRecord.getYears() != null);
			this.styleRecordService.save(styleRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un registro profesional de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del styleRecord no existe
	 */
	public void deleteStyleRecord(String username, Integer styleRecordId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			StyleRecord styleRecord = this.styleRecordService.findOne(styleRecordId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(styleRecord.getCurricula()));

			this.styleRecordService.delete(styleRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void createStyleRecordDriver() {

		final Object testingData[][] = {
			// Creacion de registro profesional sin autentificarse -> false
			{
				null, 527, 2, 516, IllegalArgumentException.class
			},
			// Creacion de registro profesional como autentificado (1) -> false
			{
				"admin", 527, 2, 516, IllegalArgumentException.class
			},
			// Creacion de registro profesional autentificado (2) -> false
			{
				"academy1", 527, 2, 516, IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", 527, null, 516, IllegalArgumentException.class
			},
			// Id del curriculum no existe -> false
			{
				"dancer1", 9999, 2, 516, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 527, 2, 516, IllegalArgumentException.class
			},
			// Id del style no existe -> false
			{
				"dancer1", 527, 2, 9999, IllegalArgumentException.class
			},
			// Creacion de registro profesional correcto-> true
			{
				"dancer1", 527, 2, 516, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createStyleRecord((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Integer) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void editStyleRecordDriver() {

		final Object testingData[][] = {
			// Edicion de registro profesional sin autentificarse -> false
			{
				null, 533, 2, 516, IllegalArgumentException.class
			},
			// Edicion de registro profesional como autentificado (1) -> false
			{
				"admin", 533, 2, 516, IllegalArgumentException.class
			},
			//Edicion de registro profesional autentificado (2) -> false
			{
				"academy1", 533, 2, 516, IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", 533, null, 516, IllegalArgumentException.class
			},
			// Id del styleRecord no existe -> false
			{
				"dancer1", 9999, 2, 516, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 533, 2, 516, IllegalArgumentException.class
			},
			// Id del style no existe -> false
			{
				"dancer1", 533, 2, 9999, IllegalArgumentException.class
			},
			// Edicion de registro profesional correcto-> true
			{
				"dancer1", 533, 2, 516, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editStyleRecord((String) testingData[i][0], (Integer) testingData[i][1], (Integer) testingData[i][2], (Integer) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void deleteStyleRecordDriver() {

		final Object testingData[][] = {
			// Delete de registro profesional sin autentificarse -> false
			{
				null, 533, IllegalArgumentException.class
			},
			// Delete de registro profesional como autentificado (1) -> false
			{
				"admin", 533, IllegalArgumentException.class
			},
			// Delete de registro profesional autentificado (2) -> false
			{
				"academy1", 533, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 533, IllegalArgumentException.class
			},
			// Id del curriculum no existe -> false
			{
				"dancer1", 9999, IllegalArgumentException.class
			},
			// Delete de registro profesional correcto-> true
			{
				"dancer1", 533, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteStyleRecord((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
