package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.EndorserRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class EndorserRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private DancerService dancerService;

	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private EndorserRecordService	endorserRecordService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la creacion de una referencia en un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El id del curriculum no existe
	 * � El curriculum no pertenece al dancer logueado
	 */
	public void createEndorserRecord(String username, String fullName, String contactMeans, Integer curriculaId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			EndorserRecord endorserRecord = this.endorserRecordService.create(this.curriculaService.findOne(curriculaId));
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(endorserRecord.getCurricula()));

			endorserRecord.setFullName(fullName);

			String[] contactMeansArray = contactMeans.split(",");

			Collection<String> contactMeansCollection = new ArrayList<String>();

			for (String s : contactMeansArray)
				contactMeansCollection.add(s);

			endorserRecord.setContactMeans(contactMeansCollection);

			this.endorserRecordService.comprobacion(endorserRecord);
			this.endorserRecordService.save(endorserRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la edicion de una referencia de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del endorserRecord no existe
	 */
	public void editEndorserRecord(String username, Integer endorserRecordId, String fullName, String contacsMeans, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(endorserRecord.getCurricula()));

			endorserRecord.setFullName(fullName);

			String[] contactMeansArray = contacsMeans.split(",");

			Collection<String> contactMeansCollection = new ArrayList<String>();

			for (String s : contactMeansArray)
				contactMeansCollection.add(s);

			endorserRecord.setContactMeans(contactMeansCollection);

			this.endorserRecordService.comprobacion(endorserRecord);
			this.endorserRecordService.save(endorserRecord);

			this.unauthenticate();


		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo el borrado de una referencia de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del customRecord no existe
	 */
	public void deleteCustomRecord(String username, Integer endorserRecordId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(endorserRecord.getCurricula()));

			this.endorserRecordService.delete(endorserRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void createEndorserRecordDriver() {

		final Object testingData[][] = {
			// Creacion de una referencia sin autentificarse -> false
			{
				null, "Full name", "http://www.link.com", 527, IllegalArgumentException.class
			},
			// Creacion de una referencia como autentificado (1) -> false
			{
				"admin", "Full name", "http://www.link.com", 527, IllegalArgumentException.class
			},
			// Creacion de una referencia autentificado (2) -> false
			{
				"academy1", "Full name", "http://www.link.com", 527, IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", "", "http://www.link.com", 527, IllegalArgumentException.class
			},
			// Id del curriculum no existe -> false
			{
				"dancer1", "Full name", "http://www.link.com", 99, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", "Full name", "http://www.link.com", 527, IllegalArgumentException.class
			},
			// Creacion de una referencia correcto-> true
			{
				"dancer1", "Full name", "http://www.link.com", 527, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createEndorserRecord((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Integer) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void editEndorserRecordDriver() {

		final Object testingData[][] = {
			// Edicion de una referencia sin autentificarse -> false
			{
				null, 531, "Full name", "http://www.link.com", IllegalArgumentException.class
			},
			// Edicion de una referencia como autentificado (1) -> false
			{
				"admin", 531, "Full name", "http://www.link.com", IllegalArgumentException.class
			},
			//Edicion de una referencia autentificado (2) -> false
			{
				"academy1", 531, "Full name", "http://www.link.com", IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", 531, "", "http://www.link.com", IllegalArgumentException.class
			},
			// Id del endorserRecord no existe -> false
			{
				"dancer1", 9999, "Full name", "http://www.link.com", IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 531, "Full name", "http://www.link.com", IllegalArgumentException.class
			},
			// Edicion de una referencia correcto-> true
			{
				"dancer1", 531, "Full name", "http://www.link.com", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editEndorserRecord((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void deleteEndorserRecordDriver() {

		final Object testingData[][] = {
			// Delete de una referencia sin autentificarse -> false
			{
				null, 531, IllegalArgumentException.class
			},
			// Delete de una referencia como autentificado (1) -> false
			{
				"admin", 531, IllegalArgumentException.class
			},
			// Delete de una referencia autentificado (2) -> false
			{
				"academy1", 531, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 531, IllegalArgumentException.class
			},
			// Id de la referencia no existe -> false
			{
				"dancer1", 9999, IllegalArgumentException.class
			},
			// Delete de una referencia correcto-> true
			{
				"dancer1", 531, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteCustomRecord((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
