
package services;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Apply;
import domain.Course;
import domain.Curricula;
import domain.Dancer;
import domain.Style;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class ApplyServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private DancerService	dancerService;
	@Autowired
	private AcademyService	academyService;

	@Autowired
	private ApplyService	applyService;

	@Autowired
	private CourseService	courseService;

	@Autowired
	private StyleService	styleService;
	// Templates --------------------------------------------------------------


	// An actor who is authenticated as a dancer must be able to: List his or her applications.
	// Comprobamos que los applications se listan correctamente y para los tests negativos vemos si salta la excepción correcta si se le pasan parámetros incorrectos.
	// Test positivo y 2 tests negativos
	@Test
	public void listApplicationsByDancer() {

		List<Dancer> dancers = (List<Dancer>) dancerService.findAll();
		Object testingData[][] = {
			{
				"dancer1", dancers.get(0).getId(), null
			}, {
				"dancer1", null, NullPointerException.class
			}, {
				"dancer1", 0, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++) {
			template((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void template(String username, Integer dancerId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);
			Dancer dancer = dancerService.findOne(dancerId);
			System.out.println("#listApplicationsByDancer");
			Assert.isTrue(dancer.getApplies() != null && !dancer.getApplies().isEmpty());
			for (Apply o : dancer.getApplies()) {
				System.out.println(o.getStatus() + "-" + o.getCourse().getTitle());
			}

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/**
	 * An actor who is authenticated as an academy must be able to: Manage their applications, which includes listing, accepting, and rejecting them.
	 *
	 * Test positivo aceptando y rechazando correctamente una solicitud
	 * Test negativo sin estar logado como academy
	 * Test negativo aportando un id de apply incorrecto
	 */

	@Test
	public void academyListAcceptRejectApply() {

		/*
		 * Se elige un academy que tenga un course con solicitudes no aceptadas
		 * para poder aceptarlas y rechazarlas posteriormente
		 */

		List<Academy> academies = (List<Academy>) academyService.findAll();
		Assert.isTrue(academies != null && !academies.isEmpty());
		Academy academy = null;
		Apply apply = null;
		for (Academy c : academies) {
			if (c.getCourses() != null && !c.getCourses().isEmpty()) {
				for (Course g : c.getCourses()) {
					if (g.getApplies() != null && !g.getApplies().isEmpty()) {
						for (Apply a : g.getApplies()) {
							if (!a.getStatus().equals("accepted")) {
								apply = a;
								academy = c;
								break;
							}
						}
					}
				}
			}
		}

		Assert.isTrue(academy != null && apply != null, "No se han encontrado academy y apply para realizar la prueba");

		/*
		 * Usamos el id del academy como id de apply incorrecto
		 */
		Integer idApply = academy.getId();

		Object testingData[][] = {
			{
				academy.getUserAccount().getUsername(), apply.getId(), null
			}, {
				"admin", null, NullPointerException.class
			}, {
				academy.getUserAccount().getUsername(), idApply, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++) {
			template2((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
		}
	}

	protected void template2(String username, Integer applyId, Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			authenticate(username);

			//Aceptar y rechazar
			Apply applyAceptada = applyService.setApplyAsAccepted(applyId);
			Assert.notNull(applyAceptada);
			Apply applyRechazada = applyService.setApplyAsRejected(applyId);
			Assert.notNull(applyRechazada);

			//Listar sus solicitudes
			Academy academy = academyService.findByPrincipal();
			Collection<Apply> applies = applyService.findAppliesFromAcademy(academy.getId());
			Assert.notNull(applies);

			unauthenticate();
		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		checkExceptions(expected, caught);
	}

	/*
	 * Apply for a course as a student or as a teacher.
	 * Applications as a teacher are ac-cepted up to one week before the course stats;
	 * applications as students are accept-ed at any time before the course ends.
	 * Teacher applications require to attach a cur-riculum.
	 *
	 * Para forzar el error pueden darse varios casos:
	 *
	 * · El usuario esta autentificado
	 * · Atributos del registro incorrectos
	 * · No aceptar las condiciones
	 * · Nombre de usuario ya existente
	 * · Contraseñas no coinciden
	 */

	public void applyToCourse(String username, int courseId, Boolean isTeacher, Integer curriculaId, Class<?> expected) {

		Class<?> caught = null;

		try {

			authenticate(username);

			// Comprobamos que este autentificado
			Assert.isTrue(username != null);

			applyService.apply(courseId, isTeacher, curriculaId);

			unauthenticate();

		} catch (Throwable oops) {

			caught = oops.getClass();

		}

		checkExceptions(expected, caught);
	}

	@Test
	public void applyToCourseDriver() {

		List<Style> styles = (List<Style>) styleService.findAll();
		//		List<Course> courses = (List<Course>) courseService.findAll();

		Calendar fechaCorrectaStart = Calendar.getInstance();
		Calendar fechaCorrectaEnd = Calendar.getInstance();

		fechaCorrectaStart.add(Calendar.DAY_OF_YEAR, 10);
		fechaCorrectaEnd.add(Calendar.DAY_OF_YEAR, 15);

		Calendar fechaMalaProfesorStart = Calendar.getInstance();
		Calendar fechaMalaProfesorEnd = Calendar.getInstance();

		fechaMalaProfesorStart.add(Calendar.DAY_OF_YEAR, 5);
		fechaMalaProfesorEnd.add(Calendar.DAY_OF_YEAR, 15);

		authenticate("academy1");
		Course cursoCorrectoAlumno = courseService.create();
		cursoCorrectoAlumno.setTitle("test");
		cursoCorrectoAlumno.setLevel("beginner");
		cursoCorrectoAlumno.setStartDate(fechaCorrectaStart.getTime());
		cursoCorrectoAlumno.setEndDate(fechaCorrectaEnd.getTime());
		cursoCorrectoAlumno.setDayWeek("monday");
		cursoCorrectoAlumno.setTime(fechaCorrectaEnd.getTime());
		cursoCorrectoAlumno.setStatus("organising");
		cursoCorrectoAlumno.setStyle(styles.get(0));
		cursoCorrectoAlumno = courseService.saveAndFlush(cursoCorrectoAlumno);

		Course cursoCorrectoAlumno2 = courseService.create();
		cursoCorrectoAlumno2.setTitle("test");
		cursoCorrectoAlumno2.setLevel("beginner");
		cursoCorrectoAlumno2.setStartDate(fechaCorrectaStart.getTime());
		cursoCorrectoAlumno2.setEndDate(fechaCorrectaEnd.getTime());
		cursoCorrectoAlumno2.setDayWeek("monday");
		cursoCorrectoAlumno2.setTime(fechaCorrectaEnd.getTime());
		cursoCorrectoAlumno2.setStatus("organising");
		cursoCorrectoAlumno2.setStyle(styles.get(0));
		cursoCorrectoAlumno2 = courseService.saveAndFlush(cursoCorrectoAlumno2);

		Course cursoCorrectoProfesor = courseService.create();
		cursoCorrectoProfesor.setTitle("test");
		cursoCorrectoProfesor.setLevel("beginner");
		cursoCorrectoProfesor.setStartDate(fechaCorrectaStart.getTime());
		cursoCorrectoProfesor.setEndDate(fechaCorrectaEnd.getTime());
		cursoCorrectoProfesor.setDayWeek("monday");
		cursoCorrectoProfesor.setTime(fechaCorrectaEnd.getTime());
		cursoCorrectoProfesor.setStatus("organising");
		cursoCorrectoProfesor.setStyle(styles.get(0));
		cursoCorrectoProfesor = courseService.saveAndFlush(cursoCorrectoProfesor);

		Course cursoErroneoProfesor = courseService.create();
		cursoErroneoProfesor.setTitle("test");
		cursoErroneoProfesor.setLevel("beginner");
		cursoErroneoProfesor.setStartDate(fechaMalaProfesorStart.getTime());
		cursoErroneoProfesor.setEndDate(fechaMalaProfesorEnd.getTime());
		cursoErroneoProfesor.setDayWeek("monday");
		cursoErroneoProfesor.setTime(fechaCorrectaEnd.getTime());
		cursoErroneoProfesor.setStatus("organising");
		cursoErroneoProfesor.setStyle(styles.get(0));
		cursoErroneoProfesor = courseService.saveAndFlush(cursoErroneoProfesor);

		//status = delivering
		Course cursoErroneoProfesor2 = courseService.create();
		cursoErroneoProfesor2.setTitle("test");
		cursoErroneoProfesor2.setLevel("beginner");
		cursoErroneoProfesor2.setStartDate(fechaCorrectaStart.getTime());
		cursoErroneoProfesor2.setEndDate(fechaCorrectaEnd.getTime());
		cursoErroneoProfesor2.setDayWeek("monday");
		cursoErroneoProfesor2.setTime(fechaCorrectaEnd.getTime());
		cursoErroneoProfesor2.setStatus("delivering");
		cursoErroneoProfesor2.setStyle(styles.get(0));
		cursoErroneoProfesor2 = courseService.saveAndFlush(cursoErroneoProfesor2);

		unauthenticate();

		authenticate("dancer1");
		applyService.apply(cursoCorrectoAlumno2.getId(), false, null);
		Dancer dancer = dancerService.findByPrincipal();
		List<Curricula> curriculas = (List<Curricula>) dancer.getCurriculas();
		Curricula curricula = curriculas.get(0);
		unauthenticate();

		final Object testingData[][] = {
			{//Solicitud correcta como alumno
				"dancer1", cursoCorrectoAlumno.getId(), false, null, null
			}, {//Solicitud erronea como alumno, apply a un curso ya solicitado
				"dancer1", cursoCorrectoAlumno2.getId(), false, null, IllegalArgumentException.class
			}, {//Solicitud correcta como profesor
				"dancer1", cursoCorrectoProfesor.getId(), true, curricula.getId(), null
			}, {//Solicitud erronea como profesor, fecha inicial inferior a una semana
				"dancer1", cursoErroneoProfesor.getId(), true, curricula.getId(), IllegalArgumentException.class
			}, {//Solicitud erronea como profesor, status = delivering
				"dancer1", cursoErroneoProfesor.getId(), true, curricula.getId(), IllegalArgumentException.class
			}, {//Solicitud erronea como profesor, curricula = null
				"dancer1", cursoCorrectoProfesor.getId(), true, null, IllegalArgumentException.class
			}, {//Solicitud erronea como profesor, usuario logado incorrecto
				"academy1", cursoCorrectoProfesor.getId(), true, curricula.getId(), IllegalArgumentException.class
			}, {//Solicitud erronea como alumno, usuario logado incorrecto
				"academy1", cursoCorrectoProfesor.getId(), true, curricula.getId(), IllegalArgumentException.class
			}, {//Solicitud erronea como profesor, id curso incorrecto
				"academy1", 0, true, curricula.getId(), IllegalArgumentException.class
			}, {//Solicitud erronea como alumno, id curso incorrecto
				"academy1", 0, true, curricula.getId(), IllegalArgumentException.class
			}, {//Solicitud erronea como profesor, curriculaId incorrecta
				"dancer1", cursoCorrectoProfesor.getId(), true, 0, IllegalArgumentException.class
			},
		};

		for (int i = 0; i < testingData.length; i++) {
			applyToCourse((String) testingData[i][0], (Integer) testingData[i][1], (Boolean) testingData[i][2], (Integer) testingData[i][3], (Class<?>) testingData[i][4]);
		}
	}

}
