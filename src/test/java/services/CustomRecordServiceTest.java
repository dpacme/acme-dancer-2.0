package services;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.CustomRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private DancerService dancerService;

	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private CustomRecordService	customRecordService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un registro personal de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El id del curriculum no existe
	 * � El curriculum no pertenece al dancer logueado
	 */
	public void createCustomRecord(String username, String title, String description, String links, Integer curriculaId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			CustomRecord customRecord = this.customRecordService.create(this.curriculaService.findOne(curriculaId));
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(customRecord.getCurricula()));

			customRecord.setTitle(title);
			customRecord.setDescription(description);

			String[] linksArray = links.split(",");

			Collection<URL> linksCollection = new ArrayList<URL>();

			for (String s : linksArray)
				linksCollection.add(new URL(s));

			customRecord.setLinks(linksCollection);

			this.customRecordService.comprobacion(customRecord);
			this.customRecordService.save(customRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un registro profesional de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del cutomRecord no existe
	 */
	public void editCustomRecord(String username, Integer customRecordId, String title, String description, String links, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			CustomRecord customRecord = this.customRecordService.findOne(customRecordId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(customRecord.getCurricula()));

			customRecord.setTitle(title);
			customRecord.setDescription(description);

			String[] linksArray = links.split(",");

			Collection<URL> linksCollection = new ArrayList<URL>();

			for (String s : linksArray)
				linksCollection.add(new URL(s));

			customRecord.setLinks(linksCollection);

			this.customRecordService.comprobacion(customRecord);
			this.customRecordService.save(customRecord);

			this.unauthenticate();


		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un registro profesional de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del customRecord no existe
	 */
	public void deleteCustomRecord(String username, Integer customRecordId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			CustomRecord customRecord = this.customRecordService.findOne(customRecordId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(customRecord.getCurricula()));

			this.customRecordService.delete(customRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void createCustomRecordDriver() {

		final Object testingData[][] = {
			// Creacion de registro personal sin autentificarse -> false
			{
				null, "Title", "Description", "http:www.link.com", 527, IllegalArgumentException.class
			},
			// Creacion de registro personal como autentificado (1) -> false
			{
				"admin", "Title", "Description", "http:www.link.com", 527, IllegalArgumentException.class
			},
			// Creacion de registro personal autentificado (2) -> false
			{
				"academy1", "Title", "Description", "http:www.link.com", 527, IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", "", "Description", "http:www.link.com", 527, IllegalArgumentException.class
			},
			// Id del curriculum no existe -> false
			{
				"dancer1", "Title", "Description", "http:www.link.com", 9999, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", "Title", "Description", "http:www.link.com", 527, IllegalArgumentException.class
			},
			// Creacion de registro personal correcto-> true
			{
				"dancer1", "Title", "Description", "http:www.link.com", 527, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createCustomRecord((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Integer) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void editCustomRecordDriver() {

		final Object testingData[][] = {
			// Edicion de registro personal sin autentificarse -> false
			{
				null, 529, "Title", "Description", "http:www.link.com", IllegalArgumentException.class
			},
			// Edicion de registro personal como autentificado (1) -> false
			{
				"admin", 529, "Title", "Description", "http:www.link.com", IllegalArgumentException.class
			},
			//Edicion de registro personal autentificado (2) -> false
			{
				"academy1", 529, "Title", "Description", "http:www.link.com", IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", 529, "", "Description", "http:www.link.com", IllegalArgumentException.class
			},
			// Id del styleRecord no existe -> false
			{
				"dancer1", 9999, "Title", "Description", "http:www.link.com", IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 529, "Title", "Description", "http:www.link.com", IllegalArgumentException.class
			},
			// Edicion de registro personal correcto-> true
			{
				"dancer1", 529, "Title", "Description", "http:www.link.com", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editCustomRecord((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void deleteStyleRecordDriver() {

		final Object testingData[][] = {
			// Delete de registro personal sin autentificarse -> false
			{
				null, 529, IllegalArgumentException.class
			},
			// Delete de registro personal como autentificado (1) -> false
			{
				"admin", 529, IllegalArgumentException.class
			},
			// Delete de registro personal autentificado (2) -> false
			{
				"academy1", 529, IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 529, IllegalArgumentException.class
			},
			// Id del registro personal no existe -> false
			{
				"dancer1", 9999, IllegalArgumentException.class
			},
			// Delete de registro personal correcto-> true
			{
				"dancer1", 529, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteCustomRecord((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
