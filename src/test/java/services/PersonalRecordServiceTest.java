package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.PersonalRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonalRecordServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private DancerService dancerService;

	@Autowired
	private PersonalRecordService	personalRecordService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la edicion de los datos personales de un curriculum
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Campos incorrectos
	 * � El curriculum no pertenece al dancer logueado
	 * � El id del personalRecord no existe
	 */
	public void editPersonalRecord(String username, Integer personalRecordId, String fullName, String email, String whatsapp, String linkedInUrl, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			PersonalRecord personalRecord = this.personalRecordService.findOne(personalRecordId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(personalRecord.getCurricula()));

			personalRecord.setFullName(fullName);
			personalRecord.setEmail(email);
			personalRecord.setWhatsapp(whatsapp);
			personalRecord.setLinkedInUrl(linkedInUrl);

			this.personalRecordService.comprobacion(personalRecord);
			this.personalRecordService.save(personalRecord);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void editPersonalRecordDriver() {

		final Object testingData[][] = {
			// Edicion de datos personales sin autentificarse -> false
			{
				null, 528, "Full name edicion", "Email", "Whatsapp", "LinkedInUrl", IllegalArgumentException.class
			},
			// Edicion de datos personales como autentificado (1) -> false
			{
				"admin", 528, "Full name edicion", "Email", "Whatsapp", "LinkedInUrl", IllegalArgumentException.class
			},
			//Edicion de datos personales autentificado (2) -> false
			{
				"academy1", 528, "Full name edicion", "Email", "Whatsapp", "LinkedInUrl", IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", 528, "", "Email", "Whatsapp", "LinkedInUrl", IllegalArgumentException.class
			},
			// Id del personalRecord no existe -> false
			{
				"dancer1", 9999, "Full name edicion", "Email", "Whatsapp", "LinkedInUrl", IllegalArgumentException.class
			},
			// El curriculum no pertenece al dancer logueado -> false
			{
				"dancer2", 528, "Full name edicion", "Email", "Whatsapp", "LinkedInUrl", IllegalArgumentException.class
			},
			// Edicion de datos personales correcto-> true
			{
				"dancer1", 528, "Full name edicion", "Email", "Whatsapp", "LinkedInUrl", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editPersonalRecord((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
