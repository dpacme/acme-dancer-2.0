package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.PersonalRecord;
import domain.Style;
import domain.StyleRecord;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CurriculaServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private DancerService dancerService;

	@Autowired
	private CurriculaService		curriculaService;

	@Autowired
	private PersonalRecordService	personalRecordService;

	@Autowired
	private StyleRecordService		styleRecordService;

	@Autowired
	private StyleService			styleService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un curriculum en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � Atributos incorrectos
	 * � El id del estilo no existe
	 */
	public void createCurricula(String username, String fullName, String email, String whatsapp, String linkedInUrl, Integer years, Integer styleId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			Curricula curricula = this.curriculaService.create();
			PersonalRecord personalRecord = this.personalRecordService.create(curricula);
			StyleRecord styleRecord = this.styleRecordService.create(curricula);
			Style style = this.styleService.findOne(styleId);

			personalRecord.setFullName(fullName);
			personalRecord.setEmail(email);
			personalRecord.setWhatsapp(whatsapp);
			personalRecord.setLinkedInUrl(linkedInUrl);

			this.personalRecordService.comprobacion(personalRecord);

			styleRecord.setYears(years);
			styleRecord.setStyle(style);

			Assert.isTrue(styleRecord.getYears() != null);

			curricula.setPersonalRecord(personalRecord);
			curricula.getStyleRecords().add(styleRecord);

			this.curriculaService.save(curricula);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un curriculum en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 * � El id del curriculum no existe
	 * � El curriculum no pertenece al dancer logueado
	 */
	public void deleteCurricula(String username, Integer curriculaId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			Curricula curricula = this.curriculaService.findOne(curriculaId);
			Assert.isTrue(this.dancerService.findByPrincipal().getCurriculas().contains(curricula));

			this.curriculaService.delete(curricula);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as a dancer must be able to: Manage his or her curricula; this includes listing, creating, editing, and deleting them.
	 *
	 * En este caso de uso se llevara a cabo el listado de las curriculas de un dancer
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es un dancer
	 */
	public void listCurricula(String username, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.dancerService.checkIfDancer();

			Collection<Curricula> curriculas = this.dancerService.findByPrincipal().getCurriculas();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void createCurriculaDriver() {

		final Object testingData[][] = {
			// Creaci�n de curriculum sin autentificarse -> false
			{
				null, "Full name", "Email", "Whatsapp", "LinkedInUrl", 2, 516, IllegalArgumentException.class
			},
			// Creaci�n de curriculum como autentificado (1) -> false
			{
				"admin", "Full name", "Email", "Whatsapp", "LinkedInUrl", 2, 516, IllegalArgumentException.class
			},
			// Creaci�n de curriculum como autentificado (2) -> false
			{
				"academy1", "Full name", "Email", "Whatsapp", "LinkedInUrl", 2, 516, IllegalArgumentException.class
			},
			// Campos incorrectos -> false
			{
				"dancer1", "", "Email", "Whatsapp", "LinkedInUrl", 2, 516, IllegalArgumentException.class
			},
			// Id del estilo no existe -> false
			{
				"dancer1", "Full name", "Email", "Whatsapp", "LinkedInUrl", 2, 9999, IllegalArgumentException.class
			},
			// Creaci�n de course correcta-> true
			{
				"dancer1", "Full name", "Email", "Whatsapp", "LinkedInUrl", 2, 516, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createCurricula((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Integer) testingData[i][5], (Integer) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	@Test
	public void deleteCurriculaDriver() {

		final Object testingData[][] = {
			// Borrado de curriculum sin autentificarse -> false
			{
				null, 527, IllegalArgumentException.class
			},
			// Borrado de curriculum como autentificado (1) -> false
			{
				"admin", 527, IllegalArgumentException.class
			},
			// Borrado de curriculum como autentificado (2) -> false
			{
				"academy1", 527, IllegalArgumentException.class
			},
			// El id de la curricula no existe -> false
			{
				"dancer1", 9999, IllegalArgumentException.class
			},
			// La curricula no pertenece al dancer logueado -> false
			{
				"dancer2", 527, IllegalArgumentException.class
			},
			// Borrado de course correcta-> true
			{
				"dancer1", 527, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteCurricula((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void listCurriculaDriver() {

		final Object testingData[][] = {
			// List de curriculum sin autentificarse -> false
			{
				null, IllegalArgumentException.class
			},
			// List de curriculum como autentificado (1) -> false
			{
				"admin", IllegalArgumentException.class
			},
			// List de curriculum como autentificado (2) -> false
			{
				"academy1", IllegalArgumentException.class
			},
			// List de course correcta-> true
			{
				"dancer1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listCurricula((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
