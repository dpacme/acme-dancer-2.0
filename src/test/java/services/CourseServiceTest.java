
package services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Course;
import domain.Style;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CourseServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private CourseService courseService;

	@Autowired
	private AcademyService	academyService;

	@Autowired
	private StyleService	styleService;

	// Templates --------------------------------------------------------------


	// An actor who is not authenticated/authenticated must be able to: Search for a course using a single keyword that must appear somewhere in its title or the name or the description of the corresponding style.
	// Comprobamos que se puede buscar un course por una keyword correctamente y para los tests negativos vemos si salta la excepci�n correcta si pasamos parametros nulos.
	// Test positivo y 1 test negativo
	protected void template2(String username, String keyword, final Class<?> expected) {
		Class<?> caught;

		caught = null;
		try {
			System.out.println("#searchCourse");
			List<Course> courses = (List<Course>) this.courseService.searchCourses(keyword, null, null);
			Assert.isTrue(courses != null && !courses.isEmpty());
			for (Course o : courses)
				System.out.println(o.getTitle() + "-" + o.getLevel());

		} catch (Throwable oops) {
			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an academy must be able to: Manage their courses, which includes listing, editing, or deleting them.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un curso en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una academia
	 * � Atributos incorrectos
	 * � La fecha de comienzo es posterior a la de fin
	 * � El id del estilo no existe
	 */
	public void createCourse(String username, String title, String level, String startDate, String endDate, String dayWeek, String time, Integer styleId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.academyService.checkIfAcademy();

			Course course = this.courseService.create();
			Style style = this.styleService.findOne(styleId);

			course.setTitle(title);
			course.setLevel(level);
			course.setStartDate(new Date(startDate));
			course.setEndDate(new Date(endDate));
			course.setDayWeek(dayWeek);
			course.setTime(new Date(time));
			course.setStyle(style);

			this.courseService.comprobacion(course);
			this.courseService.save(course);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an academy must be able to: Manage their courses, which includes listing, editing, or deleting them.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un curso en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una academia
	 * � El id del curso no existe
	 * � El curso no pertenece a la academia logueada
	 * � Atributos incorrectos
	 * � La fecha de comienzo es posterior a la de fin
	 * � El id del estilo no existe
	 */
	public void editCourse(String username, Integer courseId, String title, String level, String startDate, String endDate, String dayWeek, String time, Integer styleId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.academyService.checkIfAcademy();

			Course course = this.courseService.findOne(courseId);
			Style style = this.styleService.findOne(styleId);

			Assert.isTrue(this.academyService.findByPrincipal().getCourses().contains(course));

			course.setTitle(title);
			course.setLevel(level);
			course.setStartDate(new Date(startDate));
			course.setEndDate(new Date(endDate));
			course.setDayWeek(dayWeek);
			course.setTime(new Date(time));
			course.setStyle(style);

			this.courseService.comprobacion(course);
			this.courseService.save(course);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an academy must be able to: Manage their courses, which includes listing, editing, or deleting them.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un curso en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una academia
	 * � El id del curso no existe
	 * � El curso no pertenece a la academia logueada
	 */
	public void deleteCourse(String username, Integer courseId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.academyService.checkIfAcademy();

			Course course = this.courseService.findOne(courseId);

			Assert.isTrue(this.academyService.findByPrincipal().getCourses().contains(course));

			this.courseService.delete(course);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an academy must be able to: Manage their courses, which includes listing, editing, or deleting them.
	 *
	 * En este caso de uso se llevara a cabo el listado de los cursos de una academia
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una academia
	 */
	public void listMyCourse(String username, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.academyService.checkIfAcademy();

			Collection<Course> courses = this.academyService.findByPrincipal().getCourses();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Browse the catalogue of courses and navigate to the academies that offer them.
	 *
	 * Se prueba el listado con varios actores logados y la obtenci�n de los cursos de una academ�a.
	 */
	public void catalogueCourse(String username, Class<?> expected) {

		Class<?> caught = null;

		try {
			if (username != null)
				this.authenticate(username);

			List<Course> courses = (List<Course>) this.courseService.findAll();

			System.out.println("###catalogueCourse##");
			System.out.println("\nCourses:\n");
			for (Course c : courses)
				System.out.println("Tittle=" + c.getTitle() + "\n");

			if (courses != null && !courses.isEmpty()) {
				Course course = this.courseService.findOne(courses.get(0).getId());
				Assert.notNull(course);
				Academy academy = course.getAcademy();
				System.out.println("Academy of course " + course.getTitle() + " = " + academy.getName());
			}
			if (username != null)
				this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void searchCourse() {

		final Object testingData[][] = {
			{
				"", "1", null
			}, {
				"", null, IllegalArgumentException.class
			}
		};

		for (int i = 0; i < testingData.length; i++)
			this.template2((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void createCourseDriver() {

		final Object testingData[][] = {
			// Creaci�n de course sin autentificarse -> false
			{
				null, "Title", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Creaci�n de course como autentificado (1) -> false
			{
				"admin", "Title", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Creaci�n de course como autentificado (2) -> false
			{
				"dancer1", "Title", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Creaci�n de course on atributos incorrectos -> false
			{
				"academy1", "", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Creaci�n de course con fecha de inicio posterior a la de fin -> false
			{
				"academy1", "Title", "beginner", "11/15/2018", "10/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Id del estilo no existe -> false
			{
				"academy1", "Title", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 9999, IllegalArgumentException.class
			},
			// Creaci�n de course correcta-> true
			{
				"academy1", "Title", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createCourse((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (Integer) testingData[i][7],
				(Class<?>) testingData[i][8]);
	}

	@Test
	public void editCourseDriver() {

		final Object testingData[][] = {
			// Edicion de course sin autentificarse -> false
			{
				null, 551, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Edicion de course como autentificado (1) -> false
			{
				"admin", 551, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Edicion de course como autentificado (2) -> false
			{
				"dancer1", 551, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Id del curso no existe -> false
			{
				"academy1", 9999, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// El curso no pertenece a la academia logueada -> false
			{
				"academy2", 551, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Atributos incorrectos -> false
			{
				"academy1", 551, "", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Edicion de course con fecha de inicio posterior a la de fin -> false
			{
				"academy1", 551, "Title edicion", "beginner", "11/15/2018", "10/15/2018", "monday", "01/01/1965 10:00", 516, IllegalArgumentException.class
			},
			// Id del estilo no existe -> false
			{
				"academy1", 551, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 9999, IllegalArgumentException.class
			},
			// Creaci�n de course correcta-> true
			{
				"academy1", 551, "Title edicion", "beginner", "10/15/2018", "11/15/2018", "monday", "01/01/1965 10:00", 516, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editCourse((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Integer) testingData[i][8], (Class<?>) testingData[i][9]);
	}

	@Test
	public void deleteCourseDriver() {

		final Object testingData[][] = {
			// Delete de course sin autentificarse -> false
			{
				null, 551, IllegalArgumentException.class
			},
			// Delete de course como autentificado (1) -> false
			{
				"admin", 551, IllegalArgumentException.class
			},
			// Delete de course como autentificado (2) -> false
			{
				"dancer1", 551, IllegalArgumentException.class
			},
			// Id del curso no existe -> false
			{
				"academy1", 9999, IllegalArgumentException.class
			},
			// El curso no pertenece a la academia logueada -> false
			{
				"academy2", 551, IllegalArgumentException.class
			},
			// Delete de course correcto-> true
			{
				"academy1", 551, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteCourse((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void listMyCourseDriver() {

		final Object testingData[][] = {
			// List de courses sin autentificarse -> false
			{
				null, IllegalArgumentException.class
			},
			// List de courses como autentificado (1) -> false
			{
				"admin", IllegalArgumentException.class
			},
			// List de courses como autentificado (2) -> false
			{
				"dancer1", IllegalArgumentException.class
			},
			// List de courses correcto-> true
			{
				"academy1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listMyCourse((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void catalogueCourseDriver() {

		final Object testingData[][] = {
			{
				"admin", null
			}, {
				"dancer1", null
			}, {
				"academy1", null
			},//Test negativo, usuario inexistente
			{
				"manager1", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.catalogueCourse((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
