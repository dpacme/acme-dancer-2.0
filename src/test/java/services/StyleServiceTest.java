package services;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import domain.Style;
import utilities.AbstractTest;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class StyleServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private StyleService			styleService;

	@Autowired
	private AdministratorService	administratorService;

	// Templates --------------------------------------------------------------


	/*
	 * An actor who is authenticated as an administrator must be able to: Manage the taxonomy of styles, which includes listing, creating, editing, or deleting them. A style can be deleted as long as its not taught in any courses.
	 *
	 * En este caso de uso se llevara a cabo la creacion de un estilo en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una administrador
	 * � Atributos incorrectos
	 */
	public void createStyle(String username, String name, String description, String pictures, String videos, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.administratorService.checkIfAdministrator();

			Style style = this.styleService.create();

			style.setName(name);
			style.setDescription(description);

			String[] picturesArray = pictures.split(",");
			String[] videosArray = videos.split(",");

			Collection<URL> picturesCollection = new ArrayList<URL>();
			Collection<URL> videosCollection = new ArrayList<URL>();

			for (String s : picturesArray)
				picturesCollection.add(new URL(s));

			for (String s : videosArray)
				videosCollection.add(new URL(s));

			this.styleService.comprobacion(style);
			this.styleService.save(style);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Manage the taxonomy of styles, which includes listing, creating, editing, or deleting them. A style can be deleted as long as its not taught in any courses.
	 *
	 * En este caso de uso se llevara a cabo la edicion de un estilo en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una administrador
	 * � El id del estilo no existe
	 * � Atributos incorrectos
	 */
	public void editStyle(String username, Integer styleId, String name, String description, String pictures, String videos, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.administratorService.checkIfAdministrator();

			Style style = this.styleService.findOne(styleId);

			style.setName(name);
			style.setDescription(description);

			String[] picturesArray = pictures.split(",");
			String[] videosArray = videos.split(",");

			Collection<URL> picturesCollection = new ArrayList<URL>();
			Collection<URL> videosCollection = new ArrayList<URL>();

			for (String s : picturesArray)
				picturesCollection.add(new URL(s));

			for (String s : videosArray)
				videosCollection.add(new URL(s));

			this.styleService.comprobacion(style);
			this.styleService.save(style);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Manage the taxonomy of styles, which includes listing, creating, editing, or deleting them. A style can be deleted as long as its not taught in any courses.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un estilo en el sistema
	 * Para forzar el error pueden darse varios casos:
	 *
	 * � El usuario no esta autentificado
	 * � El usuario autentificado no es una administrador
	 * � El id del estilo no existe
	 * � El estilo esta asignado a algun recurso
	 */
	public void deleteStyle(String username, Integer styleId, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username != null);

			this.administratorService.checkIfAdministrator();

			Style style = this.styleService.findOne(styleId);

			Assert.isTrue(style.getCourses().isEmpty() || style.getCourses() == null);

			this.styleService.delete(style);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * An actor who is authenticated as an administrator must be able to: Manage the taxonomy of styles, which includes listing, creating, editing, or deleting them. A style can be deleted as long as its not taught in any courses.
	 *
	 * En este caso de uso se llevara a cabo el borrado de un estilo en el sistema
	 * Para este caso de uso no existen restricciones ni se utilizan ids que puedan provocar fallos
	 *
	 */
	public void listStyles(String username, Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Collection<Style> styles = this.styleService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void createStyleDriver() {

		final Object testingData[][] = {
			// Creaci�n de style sin autentificarse -> false
			{
				null, "Name", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Creaci�n de style como autentificado (1) -> false
			{
				"academy1", "Name", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Creaci�n de style como autentificado (2) -> false
			{
				"dancer1", "Name", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Creaci�n de style on atributos incorrectos -> false
			{
				"admin", "", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Creaci�n de style correcta-> true
			{
				"admin", "Name", "Description", "http://www.picture.com", "http://www.video.com", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.createStyle((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void editStyleDriver() {

		final Object testingData[][] = {
			// Edicion de style sin autentificarse -> false
			{
				null, 516, "Name edition", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Edicion de style como autentificado (1) -> false
			{
				"academy1", 516, "Name edition", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Edicion de style como autentificado (2) -> false
			{
				"dancer1", 516, "Name edition", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// El id del estilo no existe -> false
			{
				"dancer1", 9999, "Name edition", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// Edicion de style on atributos incorrectos -> false
			{
				"admin", 516, "", "Description", "http://www.picture.com", "http://www.video.com", IllegalArgumentException.class
			},
			// CreaEdicionci�n de style correcta-> true
			{
				"admin", 516, "Name edition", "Description", "http://www.picture.com", "http://www.video.com", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.editStyle((String) testingData[i][0], (Integer) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	@Test
	public void deleteStyleDriver() {

		final Object testingData[][] = {
			// Delete de style sin autentificarse -> false
			{
				null, 516, IllegalArgumentException.class
			},
			// Delete de style como autentificado (1) -> false
			{
				"academy1", 516, IllegalArgumentException.class
			},
			// Delete de style como autentificado (2) -> false
			{
				"dancer1", 516, IllegalArgumentException.class
			},
			// Id del curso no existe -> false
			{
				"admin", 9999, IllegalArgumentException.class
			},
			// El estilo esta asociado a uno o mas cursos -> false
			{
				"admin", 516, IllegalArgumentException.class
			},
			// Delete de style correcto-> true
			{
				"admin", 519, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteStyle((String) testingData[i][0], (Integer) testingData[i][1], (Class<?>) testingData[i][2]);
	}

	@Test
	public void listMyCourseDriver() {

		final Object testingData[][] = {
			// List de styles sin autentificarse -> true
			{
				null, null
			},
			// List de styles como autentificado (1) -> true
			{
				"admin", null
			},
			// List de styles como autentificado (2) -> true
			{
				"dancer1", null
			},
			// List de styles correcto-> true
			{
				"academy1", null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listStyles((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
