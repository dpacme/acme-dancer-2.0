
package services;

import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Academy;
import domain.Apply;
import domain.Course;
import domain.Curricula;
import domain.Dancer;
import repositories.ApplyRepository;

@Service
@Transactional
public class ApplyService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private ApplyRepository applyRepository;


	// Constructor methods --------------------------------------------------------------
	public ApplyService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private AcademyService		academyService;

	@Autowired
	private CourseService		courseService;

	@Autowired
	private DancerService		dancerService;

	@Autowired
	private CurriculaService	curriculaService;


	// Simple CRUD methods --------------------------------------------------------------

	public Apply findOne(int applyId) {
		Assert.isTrue(applyId != 0);
		Apply result;

		result = applyRepository.findOne(applyId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Apply> findAll() {
		Collection<Apply> result;

		result = applyRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Apply apply) {
		Assert.notNull(apply);
		applyRepository.save(apply);
	}

	public Apply create() {
		Apply apply = new Apply();
		return apply;
	}

	public void delete(Apply apply) {
		Assert.notNull(apply);
		applyRepository.delete(apply);
	}

	public Apply saveAndFlush(Apply apply) {
		Assert.notNull(apply);

		return applyRepository.saveAndFlush(apply);
	}

	// Other bussines methods -----------------------------------------------------

	public Apply setApplyAsPending(int applyID) {
		Academy academy = academyService.findByPrincipal();
		Assert.notNull(academy);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getCourse().getAcademy().getId() == academy.getId());

		apply.setStatus("pending");
		return saveAndFlush(apply);
	}

	public Apply setApplyAsAccepted(int applyID) {
		Academy academy = academyService.findByPrincipal();
		Assert.notNull(academy);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getCourse().getAcademy().getId() == academy.getId());

		/*
		 * Se comprueba que no se acepte un apply de teacher si se encuentra en estado delivering
		 * y que no se acepte un apply de student si se encuentra en estado organising
		 */
		Course course = apply.getCourse();

		boolean tienePermiso = true;

		if ((course.getStatus().equals("organising") || course.getStatus().equals("Organising") && apply.getCurricula() == null) || (course.getStatus().equals("delivering") || course.getStatus().equals("Delivering") && apply.getCurricula() != null)) {
			tienePermiso = false;
		}

		Assert.isTrue(tienePermiso);

		apply.setStatus("accepted");
		return saveAndFlush(apply);
	}

	public Apply setApplyAsRejected(int applyID) {
		Academy academy = academyService.findByPrincipal();
		Assert.notNull(academy);

		Apply apply = findOne(applyID);
		Assert.notNull(apply);

		//Se comprueba que el apply pertenece a una oferta de la compa��a logada
		Assert.isTrue(apply.getCourse().getAcademy().getId() == academy.getId());

		apply.setStatus("rejected");
		return saveAndFlush(apply);
	}

	//QUERY - The minimum, the average, the standard deviation, and the maximum num-ber of applications per course.
	public Collection<Object> minMaxAvgAndStadevOfAppliesPerCourse() {
		return applyRepository.minMaxAvgAndStadevOfAppliesPerCourse();
	}

	public void apply(int courseId, Boolean isTeacher, Integer curriculaId) {
		Dancer dancer = dancerService.findByPrincipal();
		Assert.notNull(dancer);
		Course course = courseService.findOne(courseId);
		Assert.notNull(course);

		Assert.isTrue(existApplyForCourseByDancer(courseId, dancer.getId()) == 0);

		Date currentDate = new Date();
		DateTime dateTimeCurrent = new DateTime(currentDate.getTime());
		DateTime dateTimeStart = new DateTime(course.getStartDate().getTime());
		DateTime dateTimeEnd = new DateTime(course.getEndDate().getTime());

		if (isTeacher) {
			Assert.isTrue(Days.daysBetween(dateTimeCurrent, dateTimeStart).getDays() > 7 && ((course.getStatus().compareTo("Organising") == 0) || (course.getStatus().compareTo("organising") == 0)));
		}
		Assert.isTrue(Days.daysBetween(dateTimeCurrent, dateTimeEnd).getDays() > 0);

		Apply apply = create();
		apply.setCourse(course);
		apply.setDancer(dancer);
		apply.setStatus("pending");
		Date date = new Date();
		apply.setMomment(date);

		if (isTeacher) {
			Assert.notNull(curriculaId);
			Curricula curricula = curriculaService.findOne(curriculaId);
			Assert.notNull(curricula);
			apply.setCurricula(curriculaService.clone(curricula.getId()));
		}

		save(apply);
	}

	public Integer existApplyForCourseByDancer(int courseId, int dancerId) {
		return applyRepository.existApplyForCourseByDancer(courseId, dancerId);
	}

	Collection<Apply> findAppliesFromAcademy(Integer academyId) {
		Academy academy = this.academyService.findOne(academyId);
		Assert.notNull(academy);
		Collection<Apply> result = this.applyRepository.findAppliesFromAcademy(academyId);
		return result;
	}

}
