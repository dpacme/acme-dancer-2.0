package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.CustomRecord;
import repositories.CustomRecordRepository;

@Service
@Transactional
public class CustomRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CustomRecordRepository customRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public CustomRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public CustomRecord findOne(int customRecordId) {
		Assert.isTrue(customRecordId != 0);
		CustomRecord result;

		result = this.customRecordRepository.findOne(customRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<CustomRecord> findAll() {
		Collection<CustomRecord> result;

		result = this.customRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(CustomRecord customRecord) {
		Assert.notNull(customRecord);
		this.customRecordRepository.save(customRecord);
	}

	public CustomRecord create(Curricula curricula) {
		CustomRecord customRecord = new CustomRecord();
		customRecord.setCurricula(curricula);
		return customRecord;
	}

	public void delete(CustomRecord customRecord) {
		Assert.notNull(customRecord);
		this.customRecordRepository.delete(customRecord);
	}

	public CustomRecord saveAndFlush(CustomRecord customRecord) {
		Assert.notNull(customRecord);

		return this.customRecordRepository.saveAndFlush(customRecord);
	}

	// Other bussines methods -----------------------------------------------------

	public void comprobacion(CustomRecord customRecord) {
		Assert.isTrue(customRecord.getTitle() != "");
		Assert.isTrue(customRecord.getDescription() != "");
	}
}
