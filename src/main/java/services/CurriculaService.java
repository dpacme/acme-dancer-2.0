
package services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.CustomRecord;
import domain.Dancer;
import domain.EndorserRecord;
import domain.PersonalRecord;
import domain.StyleRecord;
import forms.CurriculaForm;
import repositories.CurriculaRepository;

@Service
@Transactional
public class CurriculaService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CurriculaRepository curriculaRepository;


	// Constructor methods --------------------------------------------------------------
	public CurriculaService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private DancerService			dancerService;

	@Autowired
	private EndorserRecordService	endorserRecordService;

	@Autowired
	private CustomRecordService		customRecordService;

	@Autowired
	private StyleRecordService		styleRecordService;

	@Autowired
	private PersonalRecordService	personalRecordService;


	// Simple CRUD methods --------------------------------------------------------------

	public Curricula findOne(int curriculaId) {
		Assert.isTrue(curriculaId != 0);
		Curricula result;

		result = curriculaRepository.findOne(curriculaId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Curricula> findAll() {
		Collection<Curricula> result;

		result = curriculaRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Curricula curricula) {
		Assert.notNull(curricula);
		curriculaRepository.save(curricula);
	}

	public Curricula create() {
		Curricula curricula = new Curricula();
		curricula.setTicker(createTicker());
		curricula.setDancer(dancerService.findByPrincipal());
		curricula.setIsCopy(false);
		curricula.setStyleRecords(new ArrayList<StyleRecord>());
		curricula.setCustomRecords(new ArrayList<CustomRecord>());
		curricula.setEndorserRecords(new ArrayList<EndorserRecord>());
		return curricula;
	}

	public void delete(Curricula curricula) {
		Assert.notNull(curricula);
		curriculaRepository.delete(curricula);
	}

	public Curricula saveAndFlush(Curricula curricula) {
		Assert.notNull(curricula);

		return curriculaRepository.saveAndFlush(curricula);
	}

	// Other bussines methods -----------------------------------------------------

	public Curricula reconstruct(CurriculaForm curriculaForm) {
		Curricula res = create();
		PersonalRecord personalRecord = new PersonalRecord();
		StyleRecord styleRecord = new StyleRecord();

		personalRecord.setFullName(curriculaForm.getFullName());
		personalRecord.setEmail(curriculaForm.getEmail());
		personalRecord.setWhatsapp(curriculaForm.getWhatsapp());
		personalRecord.setLinkedInUrl(curriculaForm.getLinkedInUrl());
		personalRecord.setCurricula(res);

		styleRecord.setYears(curriculaForm.getYears());
		styleRecord.setStyle(curriculaForm.getStyle());
		styleRecord.setCurricula(res);

		res.setPersonalRecord(personalRecord);
		res.getStyleRecords().add(styleRecord);

		return res;
	}

	public void compruebaCurricula(Curricula curricula) {
		Assert.isTrue(curricula.getDancer().getId() == dancerService.findByPrincipal().getId());
	}

	public Curricula clone(Integer curriculaId) {
		Assert.isTrue(curriculaId != 0);
		Curricula toClone;

		toClone = curriculaRepository.findOne(curriculaId);

		Assert.notNull(toClone);
		Dancer dancer = dancerService.findByPrincipal();

		Curricula cloned = create();
		cloned.setIsCopy(true);
		cloned.setDancer(dancer);

		PersonalRecord personalRecord = personalRecordService.create(cloned);

		personalRecord.setEmail(toClone.getPersonalRecord().getEmail());
		personalRecord.setFullName(toClone.getPersonalRecord().getFullName());
		personalRecord.setLinkedInUrl(toClone.getPersonalRecord().getLinkedInUrl());
		personalRecord.setWhatsapp(toClone.getPersonalRecord().getWhatsapp());

		cloned.setPersonalRecord(personalRecord);

		cloned = saveAndFlush(cloned);



		if (toClone.getEndorserRecords() != null && !toClone.getEndorserRecords().isEmpty()) {
			for (EndorserRecord e : toClone.getEndorserRecords()) {
				EndorserRecord endorserRecord = endorserRecordService.create(cloned);
				List<String> strings = new ArrayList<>();
				strings.addAll(e.getContactMeans());
				endorserRecord.setContactMeans(strings);
				endorserRecord.setFullName(e.getFullName());
				endorserRecordService.save(endorserRecord);
			}
		}

		if (toClone.getCustomRecords() != null && !toClone.getCustomRecords().isEmpty()) {
			for (CustomRecord e : toClone.getCustomRecords()) {
				CustomRecord customRecord = customRecordService.create(cloned);
				customRecord.setDescription(e.getDescription());
				customRecord.setLinks(e.getLinks());
				customRecord.setTitle(e.getTitle());
				customRecordService.save(customRecord);
			}
		}

		if (toClone.getStyleRecords() != null && !toClone.getStyleRecords().isEmpty()) {
			for (StyleRecord e : toClone.getStyleRecords()) {
				StyleRecord styleRecord = styleRecordService.create(cloned);
				styleRecord.setStyle(e.getStyle());
				styleRecord.setYears(e.getYears());
				styleRecordService.save(styleRecord);
			}
		}

		return saveAndFlush(cloned);
	}

	public Collection<Curricula> findCurriculasWithoutCopy(Integer dancerId) {
		try {
			return curriculaRepository.findCurriculasWithoutCopy(dancerId);
		} catch (Exception e) {
			return new ArrayList<Curricula>();
		}
	}

	public String createTicker() {
		String abecedario = "TRWAGMYFPDXBNJZSQVHLCKE";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String res = sdf.format(date) + "-" + RandomStringUtils.random(4, abecedario);
		return res;
	}

	public Collection<Curricula> filterCurriculas(Collection<Curricula> curriculas) {
		Collection<Curricula> res = new ArrayList<Curricula>();

		for (Curricula c : curriculas) {
			if (!c.getIsCopy()) {
				res.add(c);
			}
		}

		return res;
	}

}
