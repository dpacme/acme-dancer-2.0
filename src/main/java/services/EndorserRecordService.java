package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.EndorserRecord;
import repositories.EndorserRecordRepository;

@Service
@Transactional
public class EndorserRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private EndorserRecordRepository endorserRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public EndorserRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public EndorserRecord findOne(int endorserRecordId) {
		Assert.isTrue(endorserRecordId != 0);
		EndorserRecord result;

		result = this.endorserRecordRepository.findOne(endorserRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<EndorserRecord> findAll() {
		Collection<EndorserRecord> result;

		result = this.endorserRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.endorserRecordRepository.save(endorserRecord);
	}

	public EndorserRecord create(Curricula curricula) {
		EndorserRecord endorserRecord = new EndorserRecord();
		endorserRecord.setCurricula(curricula);
		return endorserRecord;
	}

	public void delete(EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.endorserRecordRepository.delete(endorserRecord);
	}

	public EndorserRecord saveAndFlush(EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);

		return this.endorserRecordRepository.saveAndFlush(endorserRecord);
	}

	// Other bussines methods -----------------------------------------------------

	public void comprobacion(EndorserRecord endorserRecord) {
		Assert.isTrue(endorserRecord.getFullName() != "");
		Assert.notEmpty(endorserRecord.getContactMeans());
	}
}
