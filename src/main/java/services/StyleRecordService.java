package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.StyleRecord;
import repositories.StyleRecordRepository;

@Service
@Transactional
public class StyleRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private StyleRecordRepository styleRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public StyleRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public StyleRecord findOne(int styleRecordId) {
		Assert.isTrue(styleRecordId != 0);
		StyleRecord result;

		result = this.styleRecordRepository.findOne(styleRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<StyleRecord> findAll() {
		Collection<StyleRecord> result;

		result = this.styleRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(StyleRecord styleRecord) {
		Assert.notNull(styleRecord);
		this.styleRecordRepository.save(styleRecord);
	}

	public StyleRecord create(Curricula curricula) {
		StyleRecord styleRecord = new StyleRecord();
		styleRecord.setCurricula(curricula);
		return styleRecord;
	}

	public void delete(StyleRecord styleRecord) {
		Assert.notNull(styleRecord);
		this.styleRecordRepository.delete(styleRecord);
	}

	public StyleRecord saveAndFlush(StyleRecord styleRecord) {
		Assert.notNull(styleRecord);

		return this.styleRecordRepository.saveAndFlush(styleRecord);
	}

	// Other bussines methods -----------------------------------------------------

}
