package services;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Curricula;
import domain.PersonalRecord;
import repositories.PersonalRecordRepository;

@Service
@Transactional
public class PersonalRecordService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private PersonalRecordRepository personalRecordRepository;


	// Constructor methods --------------------------------------------------------------
	public PersonalRecordService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public PersonalRecord findOne(int personalRecordId) {
		Assert.isTrue(personalRecordId != 0);
		PersonalRecord result;

		result = this.personalRecordRepository.findOne(personalRecordId);
		Assert.notNull(result);

		return result;
	}

	public Collection<PersonalRecord> findAll() {
		Collection<PersonalRecord> result;

		result = this.personalRecordRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);
		this.personalRecordRepository.save(personalRecord);
	}

	public PersonalRecord create(Curricula curricula) {
		PersonalRecord personalRecord = new PersonalRecord();
		personalRecord.setCurricula(curricula);
		return personalRecord;
	}

	public void delete(PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);
		this.personalRecordRepository.delete(personalRecord);
	}

	public PersonalRecord saveAndFlush(PersonalRecord personalRecord) {
		Assert.notNull(personalRecord);

		return this.personalRecordRepository.saveAndFlush(personalRecord);
	}

	// Other bussines methods -----------------------------------------------------

	public void comprobacion(PersonalRecord personalRecord) {
		Assert.isTrue(personalRecord.getFullName() != "");
		Assert.isTrue(personalRecord.getEmail() != "" || personalRecord.getWhatsapp() != "");
	}

}
