
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Apply;
import domain.Course;
import repositories.CourseRepository;

@Service
@Transactional
public class CourseService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private CourseRepository courseRepository;


	// Constructor methods --------------------------------------------------------------
	public CourseService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private AcademyService	academyService;
	@Autowired
	private ApplyService	applyService;


	// Simple CRUD methods --------------------------------------------------------------

	public Course findOne(final int courseId) {
		Assert.isTrue(courseId != 0);
		Course result;

		result = courseRepository.findOne(courseId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Course> findAll() {
		Collection<Course> result;

		result = courseRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Course course) {
		Assert.notNull(course);
		Assert.notNull(course.getStyle(), "No puede ser nulo");
		Assert.isTrue(course.getEndDate().after(course.getStartDate()) && !(course.getEndDate().equals(course.getStartDate())), "La fecha de comienzo debe ser antes que la de fin");
		courseRepository.save(course);
	}

	public Course create() {
		Course course = new Course();
		course.setAcademy(academyService.findByPrincipal());
		course.setApplies(new ArrayList<Apply>());
		course.setStatus("Organising");
		return course;
	}

	public void delete(final Course course) {
		Assert.notNull(course);
		for (final Apply a : course.getApplies()) {
			applyService.delete(a);
		}
		courseRepository.delete(course);
	}

	public Course saveAndFlush(final Course course) {
		Assert.notNull(course);

		return courseRepository.saveAndFlush(course);
	}

	// Other bussines methods -----------------------------------------------------

	public Collection<Course> searchCourses(final String keyword, final Integer academyId, final Integer styleId) {
		Collection<Course> result = new ArrayList<Course>();
		try {
			if (academyId != null) {
				result = courseRepository.getCoursesByKeywordAcademy("%" + keyword + "%", academyId);
			} else if (styleId != null) {
				result = courseRepository.getCoursesByKeywordStyle("%" + keyword + "%", styleId);
			} else {
				result = courseRepository.getCoursesByKeyword("%" + keyword + "%");
			}
		} catch (final Exception e) {
			return new ArrayList<Course>();
		}
		return result;
	}

	public String getDate(final Date date) {
		String dia = "";
		String mes = "";
		final Integer d = date.getDate();
		final Integer m = date.getMonth();
		final Integer anyo = date.getYear() + 1900;

		if (d.toString().length() == 1) {
			dia = "0" + d;
		} else {
			dia = d.toString();
		}

		if (m.toString().length() == 1) {
			mes = "0" + m;
		} else {
			mes = m.toString();
		}

		return anyo.toString() + "/" + mes + "/" + dia;
	}

	public String getTime(final Date date) {
		String hour = "";
		String min = "";
		final Integer h = date.getHours();
		final Integer m = date.getMinutes();

		if (h.toString().length() == 1) {
			hour = "0" + h;
		} else {
			hour = h.toString();
		}

		if (m.toString().length() == 1) {
			min = "0" + m;
		} else {
			min = m.toString();
		}

		return hour + ":" + min;
	}

	//QUERY - The minimum, the average, the standard deviation, and the maximum number of courses per academy.
	public Collection<Object> minMaxAvgAndStadevOfCoursesPerAcademy() {
		return courseRepository.minMaxAvgAndStadevOfCoursesPerAcademy();
	}

	public void comprobacion(Course course) {
		Assert.isTrue(course.getTitle() != "");
		Assert.isTrue(course.getLevel() != "");
		Assert.isTrue(course.getStartDate() != null);
		Assert.isTrue(course.getEndDate() != null);
		Assert.isTrue(course.getDayWeek() != "");
		Assert.isTrue(course.getTime() != null);
		Assert.isTrue(course.getEndDate().after(course.getStartDate()));
	}

}
