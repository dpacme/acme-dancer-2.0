
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Course;
import domain.Style;
import repositories.StyleRepository;

@Service
@Transactional
public class StyleService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private StyleRepository styleRepository;


	// Constructor methods --------------------------------------------------------------
	public StyleService() {
		super();
	}

	// Supporting services --------------------------------------------------------------

	// Simple CRUD methods --------------------------------------------------------------

	public Style findOne(int styleId) {
		Assert.isTrue(styleId != 0);
		Style result;

		result = this.styleRepository.findOne(styleId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Style> findAll() {
		Collection<Style> result;

		result = this.styleRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(Style style) {
		Assert.notNull(style);
		this.styleRepository.save(style);
	}

	public Style create() {
		Style style = new Style();
		style.setCourses(new ArrayList<Course>());
		return style;
	}

	public void delete(Style style) {
		Assert.notNull(style);
		Assert.isTrue(style.getCourses().isEmpty(), "El estilo esta asignado a uno o mas cursos");
		this.styleRepository.delete(style);
	}

	public Style saveAndFlush(Style style) {
		Assert.notNull(style);

		return this.styleRepository.saveAndFlush(style);
	}

	// Other bussines methods -----------------------------------------------------


	public Collection<Style> stylesOrderByCourses() {
		return this.styleRepository.stylesOrderByCourses();
	}

	public Collection<Style> stylesOrderByDancersWhoCanTeach() {
		return this.styleRepository.stylesOrderByDancersWhoCanTeach();
	}

	public void comprobacion(Style style) {
		Assert.isTrue(style.getName() != "");
		Assert.isTrue(style.getDescription() != "");
	}
}
