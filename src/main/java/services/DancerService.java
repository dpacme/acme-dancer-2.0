
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import domain.Actor;
import domain.Apply;
import domain.Dancer;
import forms.DancerForm;
import repositories.DancerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;

@Service
@Transactional
public class DancerService {

	// Managed Repository --------------------------------------------------------------
	@Autowired
	private DancerRepository dancerRepository;


	// Constructor methods --------------------------------------------------------------
	public DancerService() {
		super();
	}


	// Supporting services --------------------------------------------------------------

	@Autowired
	private ActorService actorService;


	// Simple CRUD methods --------------------------------------------------------------

	public Dancer findOne(final int dancerId) {
		Assert.isTrue(dancerId != 0);
		Dancer result;

		result = this.dancerRepository.findOne(dancerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Dancer> findAll() {
		Collection<Dancer> result;

		result = this.dancerRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Dancer dancer) {
		Assert.notNull(dancer);
		this.dancerRepository.save(dancer);
	}

	public Dancer create() {
		final Dancer dancer = new Dancer();
		return dancer;
	}

	public void delete(final Dancer dancer) {
		Assert.notNull(dancer);
		this.dancerRepository.delete(dancer);
	}

	public Dancer saveAndFlush(final Dancer dancer) {
		Assert.notNull(dancer);

		return this.dancerRepository.saveAndFlush(dancer);
	}

	// Other bussines methods -----------------------------------------------------

	public Dancer reconstruct(final DancerForm a) {
		final Dancer dancer = new Dancer();
		final Collection<Apply> applies = new ArrayList<Apply>();

		final UserAccount account = new UserAccount();
		account.setPassword(a.getPassword());
		account.setUsername(a.getUsername());

		dancer.setId(0);
		dancer.setVersion(0);
		dancer.setName(a.getName());
		dancer.setSurname(a.getSurname());
		dancer.setEmail(a.getEmail());
		dancer.setPhone(a.getPhone());
		dancer.setPostalAddress(a.getPostalAddress());

		dancer.setUserAccount(account);

		dancer.setApplies(applies);

		Assert.isTrue(a.getPassword().equals(a.getSecondPassword()), "Passwords do not match");

		Assert.isTrue(a.getCheckBox(), "You must accept the term and conditions");

		return dancer;
	}

	public List<String> comprobacionEditarListErrores(final Dancer dancer) {
		final List<String> listaErrores = new ArrayList<String>();
		if (dancer.getPostalAddress() != "" && !dancer.getPostalAddress().matches("^(\\d{5}$)"))
			listaErrores.add("postal");
		if (dancer.getPhone() != "" && !dancer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
			listaErrores.add("phone");

		return listaErrores;
	}

	public void saveForm(Dancer dancer) {

		Assert.notNull(dancer);
		String password;
		final Md5PasswordEncoder md5PassWordEncoder = new Md5PasswordEncoder();

		final Collection<Authority> auths = new ArrayList<>();
		final Authority auth = new Authority();
		auth.setAuthority("DANCER");
		auths.add(auth);

		password = dancer.getUserAccount().getPassword();
		password = md5PassWordEncoder.encodePassword(password, null);

		dancer.getUserAccount().setPassword(password);

		dancer.getUserAccount().setAuthorities(auths);

		dancer = this.dancerRepository.saveAndFlush(dancer);
	}

	public Dancer findByPrincipal() {
		Dancer result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.dancerRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	public Double ratioDancersWithCurriculum() {
		return this.dancerRepository.ratioDancersWithCurriculum();
	}

	public void comprobacion(final Dancer dancer) {
		if (dancer.getPostalAddress() != "")
			Assert.isTrue(dancer.getPostalAddress().matches("^(\\d{5}$)"), "The format of the postaladdress is incorrect");
		if (dancer.getPhone() != "")
			Assert.isTrue(dancer.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "The format of the indicated telephone is not correct");
		for (final Actor actor : this.actorService.findAll())
			Assert.isTrue(!actor.getUserAccount().getUsername().equals(dancer.getUserAccount().getUsername()));
	}

	public void checkIfDancer() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.DANCER))
				res = true;
		Assert.isTrue(res);
	}

}
