
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
public class Dancer extends Actor {

	// Relationships -------------------------------------------

	private Collection<Apply>	applies;
	private Collection<Curricula>	curriculas;


	@Valid
	@OneToMany(mappedBy = "dancer")
	public Collection<Apply> getApplies() {
		return this.applies;
	}

	public void setApplies(Collection<Apply> applies) {
		this.applies = applies;
	}

	@Valid
	@OneToMany(mappedBy = "dancer")
	public Collection<Curricula> getCurriculas() {
		return this.curriculas;
	}

	public void setCurriculas(Collection<Curricula> curriculas) {
		this.curriculas = curriculas;
	}

}
