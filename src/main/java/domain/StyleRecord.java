package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class StyleRecord extends DomainEntity {

	private Integer years;


	@NotNull
	@Min(0)
	public Integer getYears() {
		return years;
	}

	public void setYears(Integer years) {
		this.years = years;
	}


	// Relationships -------------------------------------------

	private Curricula curricula;
	private Style		style;


	@Valid
	@ManyToOne(optional = true)
	public Curricula getCurricula() {
		return curricula;
	}

	public void setCurricula(Curricula curricula) {
		this.curricula = curricula;
	}

	@Valid
	@ManyToOne(optional = false)
	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

}
