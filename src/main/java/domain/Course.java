
package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Access(AccessType.PROPERTY)
public class Course extends DomainEntity {

	private String	title;
	private String	level;
	private Date	startDate;
	private Date	endDate;
	private String	dayWeek;
	private Date	time;
	private String	status;


	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^(beginner|Beginner|intermediate|Intermediate|advanced|Advanced|professional|Professional)$")
	public String getLevel() {
		return this.level;
	}

	public void setLevel(final String level) {
		this.level = level;
	}

	@NotNull
	@Future
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(final Date startDate) {
		this.startDate = startDate;
	}

	@NotNull
	@Future
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^(monday|Monday|tuesday|Tuesday|wednesday|Wednesday|thursday|Thursday|friday|Friday|saturday|Saturday|sunday|Sunday)$")
	public String getDayWeek() {
		return this.dayWeek;
	}

	public void setDayWeek(final String dayWeek) {
		this.dayWeek = dayWeek;
	}

	@NotNull
	@DateTimeFormat(pattern = "HH:mm")
	public Date getTime() {
		return this.time;
	}

	public void setTime(final Date time) {
		this.time = time;
	}

	@NotBlank
	@SafeHtml(whitelistType = WhiteListType.NONE)
	@Pattern(regexp = "^(organising|Organising|delivering|Delivering)$")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}


	// Relationships -------------------------------------------

	private Style				style;
	private Academy				academy;
	private Collection<Apply>	applies;


	@Valid
	@ManyToOne(optional = false)
	public Style getStyle() {
		return this.style;
	}

	public void setStyle(final Style style) {
		this.style = style;
	}

	@Valid
	@ManyToOne(optional = false)
	public Academy getAcademy() {
		return this.academy;
	}

	public void setAcademy(final Academy academy) {
		this.academy = academy;
	}

	@Valid
	@OneToMany(mappedBy = "course")
	public Collection<Apply> getApplies() {
		return this.applies;
	}

	public void setApplies(final Collection<Apply> applies) {
		this.applies = applies;
	}

}
