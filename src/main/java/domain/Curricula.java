
package domain;

import java.util.Collection;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.SafeHtml;
import org.hibernate.validator.constraints.SafeHtml.WhiteListType;

@Entity
@Access(AccessType.PROPERTY)
public class Curricula extends DomainEntity {

	private String	ticker;
	private Boolean isCopy;


	@Pattern(regexp = "^(\\d{4}\\-\\d{2}\\-\\d{2}\\-\\w{4}$)")
	@SafeHtml(whitelistType = WhiteListType.NONE)
	public String getTicker() {
		return this.ticker;
	}

	public void setTicker(final String ticker) {
		this.ticker = ticker;
	}

	@NotNull
	public Boolean getIsCopy() {
		return this.isCopy;
	}

	public void setIsCopy(Boolean isCopy) {
		this.isCopy = isCopy;
	}

	// Relationships -------------------------------------------

	private Dancer						dancer;
	private PersonalRecord				personalRecord;
	private Collection<EndorserRecord>	endorserRecords;
	private Collection<CustomRecord>	customRecords;
	private Collection<StyleRecord>		styleRecords;

	@Valid
	@ManyToOne(optional = false)
	public Dancer getDancer() {
		return this.dancer;
	}

	public void setDancer(Dancer dancer) {
		this.dancer = dancer;
	}

	@Valid
	@OneToOne(cascade = CascadeType.ALL, optional = false)
	public PersonalRecord getPersonalRecord() {
		return this.personalRecord;
	}

	public void setPersonalRecord(PersonalRecord personalRecord) {
		this.personalRecord = personalRecord;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "curricula")
	public Collection<EndorserRecord> getEndorserRecords() {
		return this.endorserRecords;
	}

	public void setEndorserRecords(Collection<EndorserRecord> endorserRecords) {
		this.endorserRecords = endorserRecords;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "curricula")
	public Collection<CustomRecord> getCustomRecords() {
		return this.customRecords;
	}

	public void setCustomRecords(Collection<CustomRecord> customRecords) {
		this.customRecords = customRecords;
	}

	@Valid
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "curricula")
	public Collection<StyleRecord> getStyleRecords() {
		return this.styleRecords;
	}

	public void setStyleRecords(Collection<StyleRecord> styleRecords) {
		this.styleRecords = styleRecords;
	}

}
