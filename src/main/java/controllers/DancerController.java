
package controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Dancer;
import forms.DancerForm;
import services.DancerService;

@Controller
@RequestMapping("/dancer")
public class DancerController extends AbstractController {

	// Services
	@Autowired
	private DancerService dancerService;


	// Constructors -----------------------------------------------------------

	public DancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (REGISTRO) Creaci�n de una dancer
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final DancerForm dancerForm = new DancerForm();

		res = this.createFormModelAndView(dancerForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo customer
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final DancerForm dancerForm, final BindingResult binding) {
		ModelAndView res;
		Dancer dancer;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(dancerForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(dancerForm.getPostalAddress()) && !dancerForm.getPostalAddress().matches("^(\\d{5}$)")) {
				res.addObject("postal", "postal");
			}
			if (!StringUtils.isEmpty(dancerForm.getPhone()) && !dancerForm.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)")) {
				res.addObject("phone", "phone");
			}
		} else {
			try {

				dancer = dancerService.reconstruct(dancerForm);

				final List<String> errores = dancerService.comprobacionEditarListErrores(dancer);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(dancerForm);
					for (final String error : errores) {
						res.addObject(error, error);
					}
				} else {
					dancerService.saveForm(dancer);
					res = new ModelAndView("redirect:/security/login.do");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(dancerForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException")) {
					res.addObject("duplicate", "duplicate");
				}
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions")) {
					res.addObject("terms", "terms");
				}
				if (oops.getLocalizedMessage().contains("Passwords do not match")) {
					res.addObject("pass", "pass");
				}
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect")) {
					res.addObject("postal", "postal");
				}
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct")) {
					res.addObject("phone", "phone");
				}
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final DancerForm dancerForm) {
		ModelAndView res;

		res = this.createFormModelAndView(dancerForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final DancerForm dancerForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("dancer/create");
		res.addObject("dancerForm", dancerForm);
		res.addObject("message", message);

		return res;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Dancer dancer = dancerService.findByPrincipal();

		res = this.createFormModelAndView(dancer);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo dancer
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Dancer dancer, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(dancer);
			System.out.println(binding.getAllErrors());
		} else {
			try {

				final List<String> errores = dancerService.comprobacionEditarListErrores(dancer);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(dancer);
					for (final String error : errores) {
						res.addObject(error, error);
					}
				} else {
					dancerService.save(dancer);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(dancer);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect")) {
					res.addObject("postal", "postal");
				}
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct")) {
					res.addObject("phone", "phone");
				}
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(Dancer dancer) {
		ModelAndView res;

		res = this.createFormModelAndView(dancer, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(Dancer dancer, final String message) {
		ModelAndView res;

		res = new ModelAndView("dancer/edit");
		res.addObject("dancer", dancer);
		res.addObject("message", message);

		return res;
	}

}
