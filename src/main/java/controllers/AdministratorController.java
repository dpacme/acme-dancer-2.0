/*
 * AdministratorController.java
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Administrator;
import domain.Style;
import services.AdministratorService;
import services.ApplyService;
import services.CourseService;
import services.DancerService;
import services.StyleService;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Services

	@Autowired
	AdministratorService administratorService;
	
	@Autowired
	DancerService dancerService;
	
	@Autowired
	StyleService styleService;

	@Autowired
	ApplyService	applyService;

	@Autowired
	CourseService	courseService;

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Administrator administrator = administratorService.findByPrincipal();

		res = this.createFormModelAndView(administrator);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo admin
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Administrator administrator, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(administrator);
			System.out.println(binding.getAllErrors());
		} else
			try {

				final List<String> errores = administratorService.comprobacionEditarListErrores(administrator);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(administrator);
					for (final String error : errores)
						res.addObject(error, error);
				} else {
					administratorService.save(administrator);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(administrator);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect"))
					res.addObject("postal", "postal");
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct"))
					res.addObject("phone", "phone");
			}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(Administrator administrator) {
		ModelAndView res;

		res = this.createFormModelAndView(administrator, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(Administrator administrator, final String message) {
		ModelAndView res;

		res = new ModelAndView("administrator/edit");
		res.addObject("administrator", administrator);
		res.addObject("message", message);

		return res;
	}

	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard() {
		ModelAndView result;

		//The minimum, the average, the standard deviation, and the maximum number of applications per course.
		final Collection<Object> minMaxAvgAndStadevOfAppliesPerCourse = applyService.minMaxAvgAndStadevOfAppliesPerCourse();

		Integer minAppliesPerCourse = 0;
		Integer maxAppliesPerCourse = 0;
		Double avgAppliesPerCourse = 0.0;
		Double stdevAppliesPerCourse = 0.0;
		for (final Object o : minMaxAvgAndStadevOfAppliesPerCourse) {
			try {
				final Object[] x = (Object[]) o;

				if ((Integer) x[0] != null) {
					minAppliesPerCourse = (Integer) x[0];
				}
				if ((Integer) x[1] != null) {
					maxAppliesPerCourse = (Integer) x[1];
				}
				if ((Double) x[2] != null) {
					avgAppliesPerCourse = (Double) x[2];
				}
				if ((Double) x[3] != null) {
					stdevAppliesPerCourse = (Double) x[3];
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}

		//The minimum, the average, the standard deviation, and the maximum number of courses per academy.
		final Collection<Object> minMaxAvgAndStadevOfCoursesPerAcademy = courseService.minMaxAvgAndStadevOfCoursesPerAcademy();

		Integer minCoursesPerAcademy = 0;
		Integer maxCoursesPerAcademy = 0;
		Double avgCoursesPerAcademy = 0.0;
		Double stdevCoursesPerAcademy = 0.0;
		for (final Object o : minMaxAvgAndStadevOfCoursesPerAcademy) {
			try {
				final Object[] x = (Object[]) o;

				if ((Integer) x[0] != null) {
					minCoursesPerAcademy = (Integer) x[0];
				}
				if ((Integer) x[1] != null) {
					maxCoursesPerAcademy = (Integer) x[1];
				}
				if ((Double) x[2] != null) {
					avgCoursesPerAcademy = (Double) x[2];
				}
				if ((Double) x[3] != null) {
					stdevCoursesPerAcademy = (Double) x[3];
				}

			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
				
		//The ratio of dancers who have registered a curriculum.
		final Double ratioDancersWithCurriculumAux = dancerService.ratioDancersWithCurriculum();
		Double ratioDancersWithCurriculum = 0.0;
		try {

			if (ratioDancersWithCurriculumAux != null) {
				ratioDancersWithCurriculum = ratioDancersWithCurriculumAux;
			}

		} catch (final Exception e) {
			e.printStackTrace();
		}
		
		//A listing of styles in decreasing number of courses in which they're taught..
		final Collection<Style> stylesOrderByCourses = styleService.stylesOrderByCourses();
		
		//A listing of styles in decreasing number of dancers who can teach them.
		final Collection<Style> stylesOrderByDancersWhoCanTeach = styleService.stylesOrderByDancersWhoCanTeach();

			
		//*************************************************************************//
		result = new ModelAndView("administrator/dashboard");

		result.addObject("minAppliesPerCourse", minAppliesPerCourse);
		result.addObject("maxAppliesPerCourse", maxAppliesPerCourse);
		result.addObject("avgAppliesPerCourse", avgAppliesPerCourse);
		result.addObject("stdevAppliesPerCourse", stdevAppliesPerCourse);

		result.addObject("minCoursesPerAcademy", minCoursesPerAcademy);
		result.addObject("maxCoursesPerAcademy", maxCoursesPerAcademy);
		result.addObject("avgCoursesPerAcademy", avgCoursesPerAcademy);
		result.addObject("stdevCoursesPerAcademy", stdevCoursesPerAcademy);

		result.addObject("ratioDancersWithCurriculum", ratioDancersWithCurriculum+"%");
		result.addObject("stylesOrderByCourses", stylesOrderByCourses);
		result.addObject("stylesOrderByDancersWhoCanTeach", stylesOrderByDancersWhoCanTeach);

		result.addObject("requestURI", "administrator/dashboard.do");

		return result;
	}

}
