
package controllers.academy;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Academy;
import domain.Course;
import services.AcademyService;
import services.CourseService;
import services.StyleService;

@Controller
@RequestMapping("/course/academy")
public class CourseAcademyController extends AbstractController {

	// Services

	@Autowired
	private CourseService	courseService;

	@Autowired
	private AcademyService	academyService;

	@Autowired
	private StyleService	styleService;


	// Constructors -----------------------------------------------------------

	public CourseAcademyController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/myCourses", method = RequestMethod.GET)
	public ModelAndView myCourses() {
		ModelAndView result;
		Academy academy = this.academyService.findByPrincipal();

		Assert.notNull(academy);

		Collection<Course> courses = academy.getCourses();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("requestURI", "course/academy/myCourses.do");

		return result;
	}

	// Creaci�n de un curso
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		Course course = this.courseService.create();
		course.setStatus("Organising");

		res = this.createModelAndView(course);
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid Course course, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(course);
			if (course.getStyle() == null)
				res.addObject("styleError", "styleError");

			System.out.println(binding.getAllErrors());
		} else
			try {
				this.courseService.save(course);
				res = new ModelAndView("redirect:/course/academy/myCourses.do");

			} catch (Throwable oops) {
				res = this.createModelAndView(course);
				if (oops.getLocalizedMessage().contains("La fecha de comienzo debe ser antes que la de fin"))
					res.addObject("errorDate", "errorDate");
				if (oops.getLocalizedMessage().contains("No puede ser nulo"))
					res.addObject("styleError", "styleError");
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	// Edici�n de un curso
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int courseId) {
		ModelAndView res;

		try {
			Course course = this.courseService.findOne(courseId);
			Assert.isTrue(course.getAcademy().getId() == this.academyService.findByPrincipal().getId());
			res = this.createEditModelAndView(course);
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Course course, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(course);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.courseService.save(course);
				res = new ModelAndView("redirect:/course/academy/myCourses.do");

			} catch (Throwable oops) {
				res = this.createEditModelAndView(course);
				if (oops.getLocalizedMessage().contains("La fecha de comienzo debe ser antes que la de fin"))
					res.addObject("errorDate", "errorDate");
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid Course course, BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(course);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.courseService.delete(course);
				res = new ModelAndView("redirect:/course/academy/myCourses.do");

			} catch (Throwable oops) {
				res = this.createEditModelAndView(course);
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(Course course) {
		ModelAndView res;

		res = this.createModelAndView(course, null);

		return res;
	}

	protected ModelAndView createModelAndView(Course course, String message) {
		ModelAndView res;

		res = new ModelAndView("course/academy/create");
		res.addObject("course", course);
		res.addObject("styles", this.styleService.findAll());
		res.addObject("requestURI", "course/academy/create.do");
		res.addObject("message", message);

		return res;
	}

	protected ModelAndView createEditModelAndView(Course course) {
		ModelAndView res;

		res = this.createEditModelAndView(course, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(Course course, String message) {
		ModelAndView res;

		res = new ModelAndView("course/academy/edit");
		res.addObject("course", course);
		res.addObject("styles", this.styleService.findAll());
		res.addObject("requestURI", "course/academy/edit.do");
		res.addObject("message", message);

		return res;
	}
}
