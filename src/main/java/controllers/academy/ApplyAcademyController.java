
package controllers.academy;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Academy;
import domain.Apply;
import domain.Course;
import services.AcademyService;
import services.ApplyService;
import services.CourseService;

@Controller
@RequestMapping("/apply/academy")
public class ApplyAcademyController extends AbstractController {

	// Services
	@Autowired
	private ApplyService	applyService;

	@Autowired
	private CourseService	courseService;

	@Autowired
	private AcademyService	academyService;

	// Constructors -----------------------------------------------------------

	public ApplyAcademyController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/appliesFromCourse", method = RequestMethod.GET)
	public ModelAndView appliesFromCourse(@RequestParam int courseId) {
		ModelAndView result;
		Course course = courseService.findOne(courseId);

		//Comprobacion de que el curso es del academy logado
		Academy academy = academyService.findByPrincipal();

		boolean tienePermiso = academy != null && course.getAcademy().getId() == academy.getId();

		if (tienePermiso) {

			Collection<Apply> applies = course.getApplies();

			result = new ModelAndView("apply/list");
			result.addObject("applies", applies);
			result.addObject("requestURI", "apply/academy/appliesFromCourse.do");

		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/setApplyAsPending", method = RequestMethod.GET)
	public ModelAndView setApplyAsPending(@RequestParam int applyID) {
		ModelAndView result;

		Academy academy = this.academyService.findByPrincipal();
		Apply apply = applyService.findOne(applyID);

		//Se comprueba que la academy logada es la due�a de la oferta
		boolean tienePermiso = academy != null && academy.getId() == apply.getCourse().getAcademy().getId();

		if (tienePermiso) {
			this.applyService.setApplyAsPending(applyID);

			result = appliesFromCourse(apply.getCourse().getId());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/setApplyAsAccepted", method = RequestMethod.GET)
	public ModelAndView setApplyAsAccepted(@RequestParam int applyID) {
		ModelAndView result;

		Academy academy = this.academyService.findByPrincipal();
		Apply apply = applyService.findOne(applyID);

		//Se comprueba que la academy logada es la due�a de la oferta
		boolean tienePermiso = academy != null && academy.getId() == apply.getCourse().getAcademy().getId();

		/*
		 * Se comprueba que no se acepte un apply de teacher si se encuentra en estado delivering
		 * y que no se acepte un apply de student si se encuentra en estado organising
		 */
		Course course = apply.getCourse();

		if ((course.getStatus().equals("organising") || course.getStatus().equals("Organising") && apply.getCurricula() == null) || (course.getStatus().equals("delivering") || course.getStatus().equals("Delivering") && apply.getCurricula() != null)) {
			tienePermiso = false;
		}

		if (tienePermiso) {
			this.applyService.setApplyAsAccepted(applyID);

			result = appliesFromCourse(apply.getCourse().getId());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

	@RequestMapping(value = "/setApplyAsRejected", method = RequestMethod.GET)
	public ModelAndView setApplyAsRejected(@RequestParam int applyID) {
		ModelAndView result;

		Academy academy = this.academyService.findByPrincipal();
		Apply apply = applyService.findOne(applyID);

		//Se comprueba que la academy logada es la due�a de la oferta
		boolean tienePermiso = academy != null && academy.getId() == apply.getCourse().getAcademy().getId();

		if (tienePermiso) {
			this.applyService.setApplyAsRejected(applyID);

			result = appliesFromCourse(apply.getCourse().getId());
		} else {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

}
