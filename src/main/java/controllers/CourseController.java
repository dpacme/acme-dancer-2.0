
package controllers;

import java.util.Collection;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Academy;
import domain.Course;
import domain.Curricula;
import domain.Dancer;
import domain.Style;
import services.AcademyService;
import services.ApplyService;
import services.CourseService;
import services.CurriculaService;
import services.DancerService;
import services.StyleService;

@Controller
@RequestMapping("/course")
public class CourseController extends AbstractController {

	// Services
	@Autowired
	private CourseService	courseService;

	@Autowired
	private AcademyService	academyService;

	@Autowired
	private StyleService	styleService;

	@Autowired
	private DancerService	dancerService;

	@Autowired
	private CurriculaService	curriculaService;

	@Autowired
	private ApplyService		applyService;



	// Constructors -----------------------------------------------------------

	public CourseController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/coursesByAcademy", method = RequestMethod.GET)
	public ModelAndView coursesByAcademy(@RequestParam int academyId) {
		ModelAndView result;
		Academy academy = this.academyService.findOne(academyId);

		Collection<Course> courses = academy.getCourses();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("academyId", academyId);
		result.addObject("requestURI", "course/coursesByAcademy.do");

		return result;
	}

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam int courseId) {
		ModelAndView result;
		Course course = this.courseService.findOne(courseId);
		Dancer dancer;
		try {
			dancer = this.dancerService.findByPrincipal();
		} catch (Exception e) {
			dancer = null;
		}

		Date currentDate = new Date();

		DateTime dateTimeCurrent = new DateTime(currentDate.getTime());
		DateTime dateTimeStart = new DateTime(course.getStartDate().getTime());
		DateTime dateTimeEnd = new DateTime(course.getEndDate().getTime());

		int days = Days.daysBetween(dateTimeCurrent, dateTimeStart).getDays();

		Boolean isTeacher = (days > 7 && ((course.getStatus().compareTo("Organising") == 0) || (course.getStatus().compareTo("organising") == 0)));

		Boolean isStudent = (Days.daysBetween(dateTimeCurrent, dateTimeEnd).getDays() > 0);

		result = new ModelAndView("course/showDisplay");
		result.addObject("course", course);
		result.addObject("starDate", course.getStartDate());
		result.addObject("endDate", course.getEndDate());
		result.addObject("time", course.getTime());
		result.addObject("requestURI", "course/showDisplay.do");


		if (dancer != null) {
			if (this.applyService.existApplyForCourseByDancer(courseId, dancer.getId()) == 0) {
				result.addObject("isTeacher", isTeacher);
				result.addObject("isStudent", isStudent);
			}
			Collection<Curricula> curriculas = this.curriculaService.findCurriculasWithoutCopy(dancer.getId());
			if (curriculas != null && !curriculas.isEmpty()) {
				result.addObject("haveCurricula", true);
				result.addObject("allCurriculas", curriculas);
			}
		}

		return result;
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;

		Collection<Course> courses = this.courseService.findAll();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("requestURI", "course/all.do");

		return result;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST, params = "search")
	public ModelAndView searchPost(String keyword, Integer academyId, Integer styleId) {
		return this.searchCourses(keyword, academyId, styleId);
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET, params = "search")
	public ModelAndView searchGet(String keyword, Integer academyId, Integer styleId) {
		return this.searchCourses(keyword, academyId, styleId);
	}

	private ModelAndView searchCourses(String keyword, Integer academyId, Integer styleId) {
		ModelAndView result;
		Collection<Course> res;

		res = this.courseService.searchCourses(keyword, academyId, styleId);
		result = new ModelAndView("course/list");

		result.addObject("courses", res);
		if (academyId != null)
			result.addObject("academyId", academyId);
		else if (styleId != null)
			result.addObject("styleId", styleId);

		result.addObject("requestURI", "course/search.do");

		return result;
	}

	@RequestMapping(value = "/listByStyle", method = RequestMethod.GET)
	public ModelAndView listByStyle(@RequestParam int styleId) {
		ModelAndView result;
		Style style = this.styleService.findOne(styleId);

		Collection<Course> courses = style.getCourses();

		result = new ModelAndView("course/list");
		result.addObject("courses", courses);
		result.addObject("styleId", styleId);
		result.addObject("requestURI", "course/listByStyle.do");

		return result;
	}
}
