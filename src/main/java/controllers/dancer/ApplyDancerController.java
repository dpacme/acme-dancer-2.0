
package controllers.dancer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Dancer;
import services.ApplyService;
import services.DancerService;

@Controller
@RequestMapping("/apply/dancer")
public class ApplyDancerController extends AbstractController {

	// Services
	@Autowired
	private DancerService	dancerService;

	@Autowired
	private ApplyService	applyService;


	// Constructors -----------------------------------------------------------

	public ApplyDancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	@RequestMapping(value = "/myApplies", method = RequestMethod.GET)
	public ModelAndView myApplies() {
		ModelAndView result;
		Dancer dancer = dancerService.findByPrincipal();

		result = new ModelAndView("apply/list");
		result.addObject("applies", dancer.getApplies());

		result.addObject("requestURI", "apply/dancer/myApplies.do");

		return result;
	}

	@RequestMapping(value = "/applyForStudent", method = RequestMethod.GET)
	public ModelAndView applyForStudent(@RequestParam int courseId) {

		ModelAndView result;
		try {
			Dancer dancer = dancerService.findByPrincipal();
			applyService.apply(courseId, false, null);

			result = new ModelAndView("apply/list");
			result.addObject("applies", dancer.getApplies());
			result.addObject("requestURI", "apply/myApplies.do");

		} catch (Exception e) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}


	@RequestMapping(value = "/applyForTeacher", method = RequestMethod.POST)
	public ModelAndView applyOffer(@RequestParam Integer offerId, @RequestParam Integer curriculaId) {
		ModelAndView result;
		try {
			Dancer dancer = dancerService.findByPrincipal();
			applyService.apply(offerId, true, curriculaId);

			result = new ModelAndView("apply/list");
			result.addObject("applies", dancer.getApplies());
			result.addObject("requestURI", "apply/myApplies.do");
		} catch (Exception e) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;
	}

}
