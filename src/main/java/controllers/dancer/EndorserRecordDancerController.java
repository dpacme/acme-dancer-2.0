package controllers.dancer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.EndorserRecord;
import services.CurriculaService;
import services.EndorserRecordService;

@Controller
@RequestMapping("/endorserRecord/dancer")
public class EndorserRecordDancerController extends AbstractController {

	// Services

	@Autowired
	private CurriculaService curriculaService;

	@Autowired
	private EndorserRecordService	endorserRecordService;


	// Constructors -----------------------------------------------------------

	public EndorserRecordDancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------



	// --------------- Creation ----------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaId) {
		ModelAndView res;
		try {
			Curricula curricula = this.curriculaService.findOne(curriculaId);
			this.curriculaService.compruebaCurricula(curricula);
			EndorserRecord endorserRecord = this.endorserRecordService.create(curricula);

			res = new ModelAndView("endorserRecord/dancer/create");
			res.addObject("endorserRecord", endorserRecord);
			res.addObject("requestURI", "endorserRecord/dancer/create.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}
		return res;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid EndorserRecord endorserRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createCreateModelAndView(endorserRecord);
		else
			try {
				EndorserRecord e = this.endorserRecordService.saveAndFlush(endorserRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + e.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(endorserRecord, "endorserRecord.commit.error");
				res.addObject("style", "style");
			}
		return res;
	}

	// --------------- Edition ----------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int endorserRecordId) {
		ModelAndView res;
		try {
			EndorserRecord endorserRecord = this.endorserRecordService.findOne(endorserRecordId);
			this.curriculaService.compruebaCurricula(endorserRecord.getCurricula());
			res = new ModelAndView("endorserRecord/dancer/edit");
			res.addObject("endorserRecord", endorserRecord);
			res.addObject("requestURI", "endorserRecord/dancer/edit.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid EndorserRecord endorserRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(endorserRecord);
		else
			try {
				this.endorserRecordService.saveAndFlush(endorserRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + endorserRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(endorserRecord, "endorserRecord.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid EndorserRecord endorserRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(endorserRecord);
		else
			try {
				this.endorserRecordService.delete(endorserRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + endorserRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(endorserRecord, "endorserRecord.commit.error");

			}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createCreateModelAndView(EndorserRecord endorserRecord) {
		ModelAndView res;
		res = this.createCreateModelAndView(endorserRecord, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(EndorserRecord endorserRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("endorserRecord/dancer/create");
		res.addObject("endorserRecord", endorserRecord);
		res.addObject("requestURI", "endorserRecord/dancer/create.do");
		return res;
	}

	protected ModelAndView createEditModelAndView(EndorserRecord customRecord) {
		ModelAndView res;
		res = this.createEditModelAndView(customRecord, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(EndorserRecord endorserRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("endorserRecord/dancer/edit");
		res.addObject("endorserRecord", endorserRecord);
		res.addObject("requestURI", "endorserRecord/dancer/edit.do");
		return res;
	}
}
