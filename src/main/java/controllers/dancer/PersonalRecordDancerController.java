package controllers.dancer;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.PersonalRecord;
import services.CurriculaService;
import services.PersonalRecordService;

@Controller
@RequestMapping("/personalRecord/dancer")
public class PersonalRecordDancerController extends AbstractController {

	// Services

	@Autowired
	private CurriculaService curriculaService;

	@Autowired
	private PersonalRecordService	personalRecordService;


	// Constructors -----------------------------------------------------------

	public PersonalRecordDancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// --------------- Edition ----------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int curriculaId) {
		ModelAndView res;
		try {
			Curricula curricula = this.curriculaService.findOne(curriculaId);
			PersonalRecord personalRecord = curricula.getPersonalRecord();
			this.curriculaService.compruebaCurricula(curricula);
			res = new ModelAndView("personalRecord/dancer/edit");
			res.addObject("personalRecord", personalRecord);
			res.addObject("requestURI", "personalRecord/dancer/edit.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid PersonalRecord personalRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createEditModelAndView(personalRecord);
			if (personalRecord.getWhatsapp() == "" && personalRecord.getEmail() == "")
				res.addObject("whatsappEmail", "whatsappEmail");
			if (!StringUtils.isEmpty(personalRecord.getWhatsapp()) && !personalRecord.getWhatsapp().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"))
				res.addObject("whatsapp", "whatsapp");
		} else
			try {
				if (personalRecord.getWhatsapp() == "" && personalRecord.getEmail() == "")
					Assert.isTrue(false, "Indica al menos uno");
				if (personalRecord.getWhatsapp() != "")
					Assert.isTrue(personalRecord.getWhatsapp().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "whatsapp");
				this.personalRecordService.save(personalRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + personalRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(personalRecord, "personalRecord.commit.error");
				if (oops.getLocalizedMessage().contains("Indica al menos uno"))
					res.addObject("whatsappEmail", "whatsappEmail");
				if (oops.getLocalizedMessage().contains("whatsapp"))
					res.addObject("whatsapp", "whatsapp");
			}
		return res;
	}

	// Ancillary methods-------------------------------------

	protected ModelAndView createEditModelAndView(PersonalRecord personalRecord) {
		ModelAndView res;
		res = this.createEditModelAndView(personalRecord, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(PersonalRecord personalRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("personalRecord/dancer/edit");
		res.addObject("personalRecord", personalRecord);
		res.addObject("requestURI", "personalRecord/dancer/edit.do");
		return res;
	}
}
