package controllers.dancer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.CustomRecord;
import services.CurriculaService;
import services.CustomRecordService;

@Controller
@RequestMapping("/customRecord/dancer")
public class CustomRecordDancerController extends AbstractController {

	// Services

	@Autowired
	private CurriculaService curriculaService;

	@Autowired
	private CustomRecordService	customRecordService;


	// Constructors -----------------------------------------------------------

	public CustomRecordDancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------



	// --------------- Creation ----------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaId) {
		ModelAndView res;
		try {
			Curricula curricula = this.curriculaService.findOne(curriculaId);
			this.curriculaService.compruebaCurricula(curricula);
			CustomRecord customRecord = this.customRecordService.create(curricula);

			res = new ModelAndView("customRecord/dancer/create");
			res.addObject("customRecord", customRecord);
			res.addObject("requestURI", "customRecord/dancer/create.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}
		return res;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid CustomRecord customRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createCreateModelAndView(customRecord);
		else
			try {
				CustomRecord c = this.customRecordService.saveAndFlush(customRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + c.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(customRecord, "customRecord.commit.error");
				res.addObject("style", "style");
			}
		return res;
	}

	// --------------- Edition ----------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int customRecordId) {
		ModelAndView res;
		try {
			CustomRecord customRecord = this.customRecordService.findOne(customRecordId);
			this.curriculaService.compruebaCurricula(customRecord.getCurricula());
			res = new ModelAndView("customRecord/dancer/edit");
			res.addObject("customRecord", customRecord);
			res.addObject("requestURI", "customRecord/dancer/edit.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid CustomRecord customRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(customRecord);
		else
			try {
				this.customRecordService.saveAndFlush(customRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + customRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(customRecord, "customRecord.commit.error");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid CustomRecord customRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(customRecord);
		else
			try {
				this.customRecordService.delete(customRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + customRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(customRecord, "customRecord.commit.error");

			}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createCreateModelAndView(CustomRecord customRecord) {
		ModelAndView res;
		res = this.createCreateModelAndView(customRecord, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(CustomRecord customRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("customRecord/dancer/create");
		res.addObject("customRecord", customRecord);
		res.addObject("requestURI", "customRecord/dancer/create.do");
		return res;
	}

	protected ModelAndView createEditModelAndView(CustomRecord customRecord) {
		ModelAndView res;
		res = this.createEditModelAndView(customRecord, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(CustomRecord customRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("customRecord/dancer/edit");
		res.addObject("customRecord", customRecord);
		res.addObject("requestURI", "customRecord/dancer/edit.do");
		return res;
	}
}
