package controllers.dancer;

import java.util.Collection;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.PersonalRecord;
import forms.CurriculaForm;
import services.CurriculaService;
import services.DancerService;
import services.StyleService;

@Controller
@RequestMapping("/curricula/dancer")
public class CurriculaDancerController extends AbstractController {

	// Services

	@Autowired
	private CurriculaService curriculaService;

	@Autowired
	private DancerService		dancerService;

	@Autowired
	private StyleService		styleService;


	// Constructors -----------------------------------------------------------

	public CurriculaDancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	// VIEW
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<Curricula> aux = dancerService.findByPrincipal().getCurriculas();
		Collection<Curricula> curriculas = curriculaService.filterCurriculas(aux);

		result = new ModelAndView("curricula/dancer/list");
		result.addObject("curriculas", curriculas);
		result.addObject("requestURI", "curricula/dancer/list.do");

		return result;

	}

	// VIEW
	@RequestMapping(value = "/show", method = RequestMethod.GET)
	public ModelAndView show(@RequestParam int curriculaId) {
		ModelAndView result;

		try {
			Curricula curricula = curriculaService.findOne(curriculaId);
			curriculaService.compruebaCurricula(curricula);

			final PersonalRecord personalRecord = curricula.getPersonalRecord();

			result = new ModelAndView("curricula/dancer/show");
			result.addObject("curricula", curricula);
			result.addObject("personalRecord", personalRecord);
			result.addObject("styleRecords", curricula.getStyleRecords());
			result.addObject("customRecords", curricula.getCustomRecords());
			result.addObject("endorserRecords", curricula.getEndorserRecords());
			result.addObject("requestURI", "curricula/dancer/show.do");

		} catch (final Throwable oops) {
			result = new ModelAndView("misc/error");
			result.addObject("codigoError", "error.authorization");
		}

		return result;

	}

	// --------------- Creation ----------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		CurriculaForm curriculaForm;
		curriculaForm = new CurriculaForm();

		res = new ModelAndView("curricula/dancer/create");
		res.addObject("curriculaForm", curriculaForm);
		res.addObject("styles", styleService.findAll());
		res.addObject("requestURI", "curricula/dancer/create.do");

		return res;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "next")
	public ModelAndView next(@Valid CurriculaForm curriculaForm, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(curriculaForm);
			if (curriculaForm.getWhatsapp() == "" && curriculaForm.getEmail() == "") {
				res.addObject("whatsappEmail", "whatsappEmail");
			}
			if (!StringUtils.isEmpty(curriculaForm.getWhatsapp()) && !curriculaForm.getWhatsapp().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)")) {
				res.addObject("whatsapp", "whatsapp");
			}
		} else {
			try {
				if (curriculaForm.getWhatsapp() == "" && curriculaForm.getEmail() == "") {
					Assert.isTrue(false, "Indica al menos uno");
				}
				if (curriculaForm.getWhatsapp() != "") {
					Assert.isTrue(curriculaForm.getWhatsapp().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "whatsapp");
				}
				Curricula curricula = curriculaService.reconstruct(curriculaForm);
				Curricula result = curriculaService.saveAndFlush(curricula);
				res = new ModelAndView("redirect:/styleRecord/dancer/create.do?curriculaId=" + result.getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(curriculaForm, "curricula.commit.error");
				if (oops.getLocalizedMessage().contains("Indica al menos uno")) {
					res.addObject("whatsappEmail", "whatsappEmail");
				}
				if (oops.getLocalizedMessage().contains("whatsapp")) {
					res.addObject("whatsapp", "whatsapp");
				}
			}
		}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid CurriculaForm curriculaForm, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(curriculaForm);
			if (curriculaForm.getWhatsapp() == "" && curriculaForm.getEmail() == "") {
				res.addObject("whatsappEmail", "whatsappEmail");
			}
			if (!StringUtils.isEmpty(curriculaForm.getWhatsapp()) && !curriculaForm.getWhatsapp().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)")) {
				res.addObject("whatsapp", "whatsapp");
			}
		} else {
			try {
				if (curriculaForm.getWhatsapp() == "" && curriculaForm.getEmail() == "") {
					Assert.isTrue(false, "Indica al menos uno");
				}
				if (curriculaForm.getWhatsapp() != "") {
					Assert.isTrue(curriculaForm.getWhatsapp().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)"), "whatsapp");
				}
				Curricula curricula = curriculaService.reconstruct(curriculaForm);
				Curricula curriculaSaved =curriculaService.saveAndFlush(curricula);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId="+curriculaSaved.getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(curriculaForm, "curricula.commit.error");
				if (oops.getLocalizedMessage().contains("Indica al menos uno")) {
					res.addObject("whatsappEmail", "whatsappEmail");
				}
				if (oops.getLocalizedMessage().contains("whatsapp")) {
					res.addObject("whatsapp", "whatsapp");
				}
			}
		}
		return res;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public ModelAndView delete(@RequestParam int curriculaId) {
		ModelAndView res;
		try {
			Curricula curricula = curriculaService.findOne(curriculaId);
			curriculaService.compruebaCurricula(curricula);
			curriculaService.delete(curricula);
			res = new ModelAndView("redirect:/curricula/dancer/list.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createCreateModelAndView(CurriculaForm curriculaForm) {
		ModelAndView res;
		res = this.createCreateModelAndView(curriculaForm, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(CurriculaForm curriculaForm, String message2) {
		ModelAndView res;

		res = new ModelAndView("curricula/dancer/create");
		res.addObject("curriculaForm", curriculaForm);
		res.addObject("styles", styleService.findAll());
		res.addObject("requestURI", "curricula/dancer/create.do");
		return res;
	}
}
