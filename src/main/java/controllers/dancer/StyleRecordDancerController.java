package controllers.dancer;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Curricula;
import domain.StyleRecord;
import services.CurriculaService;
import services.StyleRecordService;
import services.StyleService;

@Controller
@RequestMapping("/styleRecord/dancer")
public class StyleRecordDancerController extends AbstractController {

	// Services

	@Autowired
	private CurriculaService curriculaService;

	@Autowired
	private StyleRecordService	styleRecordService;

	@Autowired
	private StyleService		styleService;


	// Constructors -----------------------------------------------------------

	public StyleRecordDancerController() {
		super();
	}

	// Methods ------------------------------------------------------------------



	// --------------- Creation ----------------------
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(@RequestParam final int curriculaId) {
		ModelAndView res;
		try {
			Curricula curricula = this.curriculaService.findOne(curriculaId);
			this.curriculaService.compruebaCurricula(curricula);
			StyleRecord styleRecord = this.styleRecordService.create(curricula);

			res = new ModelAndView("styleRecord/dancer/create");
			res.addObject("styleRecord", styleRecord);
			res.addObject("styles", this.styleService.findAll());
			res.addObject("requestURI", "styleRecord/dancer/create.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}
		return res;

	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "next")
	public ModelAndView next(@Valid StyleRecord styleRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(styleRecord);
			if (styleRecord.getStyle() == null)
				res.addObject("style", "style");
		} else
			try {
				Assert.notNull(styleRecord.getStyle());
				this.styleRecordService.saveAndFlush(styleRecord);
				res = new ModelAndView("redirect:/styleRecord/dancer/create.do?curriculaId=" + styleRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(styleRecord, "styleRecord.commit.error");
				res.addObject("style", "style");
			}
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid StyleRecord styleRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createCreateModelAndView(styleRecord);
			if (styleRecord.getStyle() == null)
				res.addObject("style", "style");
		} else
			try {
				Assert.notNull(styleRecord.getStyle());
				StyleRecord s = this.styleRecordService.saveAndFlush(styleRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + s.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createCreateModelAndView(styleRecord, "styleRecord.commit.error");
				res.addObject("style", "style");
			}
		return res;
	}

	// --------------- Edition ----------------------
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam int styleRecordId) {
		ModelAndView res;
		try {
			StyleRecord styleRecord = this.styleRecordService.findOne(styleRecordId);
			this.curriculaService.compruebaCurricula(styleRecord.getCurricula());
			res = new ModelAndView("styleRecord/dancer/edit");
			res.addObject("styleRecord", styleRecord);
			res.addObject("styles", this.styleService.findAll());
			res.addObject("requestURI", "styleRecord/dancer/edit.do");
		} catch (Throwable oops) {
			res = new ModelAndView("misc/error");
			res.addObject("codigoError", "error.authorization");
		}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid StyleRecord styleRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors()) {
			res = this.createEditModelAndView(styleRecord);
			if (styleRecord.getStyle() == null)
				res.addObject("style", "style");
		} else
			try {
				Assert.notNull(styleRecord.getStyle());
				StyleRecord s = this.styleRecordService.saveAndFlush(styleRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + s.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(styleRecord, "styleRecord.commit.error");
				res.addObject("select", "select");
			}
		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid StyleRecord styleRecord, BindingResult binding) {
		ModelAndView res;
		if (binding.hasErrors())
			res = this.createEditModelAndView(styleRecord);
		else
			try {
				this.styleRecordService.delete(styleRecord);
				res = new ModelAndView("redirect:/curricula/dancer/show.do?curriculaId=" + styleRecord.getCurricula().getId());
			} catch (Throwable oops) {
				res = this.createEditModelAndView(styleRecord, "styleRecord.commit.error");
			}
		return res;
	}

	// Ancillary methods-------------------------------------
	protected ModelAndView createCreateModelAndView(StyleRecord styleRecord) {
		ModelAndView res;
		res = this.createCreateModelAndView(styleRecord, null);
		return res;
	}

	protected ModelAndView createCreateModelAndView(StyleRecord styleRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("styleRecord/dancer/create");
		res.addObject("styleRecord", styleRecord);
		res.addObject("styles", this.styleService.findAll());
		res.addObject("requestURI", "styleRecord/dancer/create.do");
		return res;
	}

	protected ModelAndView createEditModelAndView(StyleRecord styleRecord) {
		ModelAndView res;
		res = this.createEditModelAndView(styleRecord, null);
		return res;
	}

	protected ModelAndView createEditModelAndView(StyleRecord styleRecord, String message2) {
		ModelAndView res;

		res = new ModelAndView("styleRecord/dancer/edit");
		res.addObject("styleRecord", styleRecord);
		res.addObject("styles", this.styleService.findAll());
		res.addObject("requestURI", "styleRecord/dancer/edit.do");
		return res;
	}
}
