
package controllers.administrator;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import controllers.AbstractController;
import domain.Style;
import services.StyleService;

@Controller
@RequestMapping("/style/administrator")
public class StyleAdministratorController extends AbstractController {

	// Services

	@Autowired
	private StyleService styleService;


	// Constructors -----------------------------------------------------------

	public StyleAdministratorController() {
		super();
	}

	// Methods ------------------------------------------------------------------
	/*
	 * @RequestMapping(value = "/list", method = RequestMethod.GET)
	 * public ModelAndView myCourses() {
	 * ModelAndView result;
	 *
	 * Collection<Style> styles = this.styleService.findAll();
	 *
	 * result = new ModelAndView("style/administrator/list");
	 * result.addObject("styles", styles);
	 * result.addObject("requestURI", "style/administrator/list.do");
	 *
	 * return result;
	 * }
	 */

	// Creaci�n de un estilo
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final Style style = this.styleService.create();

		res = this.createModelAndView(style);
		return res;
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final Style style, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createModelAndView(style);

			System.out.println(binding.getAllErrors());
		} else
			try {
				this.styleService.save(style);
				res = new ModelAndView("redirect:/style/list.do");

			} catch (final Throwable oops) {
				res = this.createModelAndView(style);
			}

		return res;
	}

	// Edici�n de un estilo
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam final int styleId) {
		ModelAndView res;

		final Style style = this.styleService.findOne(styleId);
		res = this.createEditModelAndView(style);

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid final Style style, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createEditModelAndView(style);
			System.out.println(binding.getAllErrors());
		} else
			try {
				this.styleService.save(style);
				res = new ModelAndView("redirect:/style/list.do");

			} catch (final Throwable oops) {
				res = this.createEditModelAndView(style);
				System.out.println(oops.getLocalizedMessage());
			}

		return res;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
	public ModelAndView delete(@Valid final Style style, final BindingResult binding) {
		ModelAndView res;

		try {
			this.styleService.delete(style);
			res = new ModelAndView("redirect:/style/list.do");

		} catch (final Throwable oops) {
			res = this.createEditModelAndView(style);
			if (oops.getLocalizedMessage().contains("El estilo esta asignado a uno o mas cursos"))
				res.addObject("deleteError", "deleteError");
			System.out.println(oops.getLocalizedMessage());
		}

		return res;
	}

	// Creaci�n de ModelAndView
	protected ModelAndView createModelAndView(final Style style) {
		ModelAndView res;

		res = this.createModelAndView(style, null);

		return res;
	}

	protected ModelAndView createModelAndView(final Style style, final String message) {
		ModelAndView res;

		res = new ModelAndView("style/administrator/create");
		res.addObject("style", style);
		res.addObject("requestURI", "style/administrator/create.do");
		res.addObject("message", message);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Style style) {
		ModelAndView res;

		res = this.createEditModelAndView(style, null);

		return res;
	}

	protected ModelAndView createEditModelAndView(final Style style, final String message) {
		ModelAndView res;

		res = new ModelAndView("style/administrator/edit");
		res.addObject("style", style);
		res.addObject("requestURI", "style/administrator/edit.do");
		res.addObject("message", message);

		return res;
	}

}
