
package controllers;

import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import domain.Academy;
import forms.AcademyForm;
import services.AcademyService;

@Controller
@RequestMapping("/academy")
public class AcademyController extends AbstractController {

	// Services
	@Autowired
	private AcademyService academyService;


	// Constructors -----------------------------------------------------------

	public AcademyController() {
		super();
	}

	// Methods ------------------------------------------------------------------

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ModelAndView all() {
		ModelAndView result;
		final Collection<Academy> res = academyService.findAll();

		result = new ModelAndView("academy/all");
		result.addObject("academies", res);
		result.addObject("requestURI", "academy/all.do");

		return result;
	}

	@RequestMapping(value = "/showDisplay", method = RequestMethod.GET)
	public ModelAndView showDisplay(@RequestParam final int academyId) {
		ModelAndView result;
		final Academy academy = academyService.findOne(academyId);

		result = new ModelAndView("academy/showDisplay");
		result.addObject("academy", academy);
		result.addObject("requestURI", "academy/showDisplay.do");
		result.addObject("courses", academy.getCourses());

		return result;
	}

	// (REGISTRO) Creaci�n de una academy
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView res;

		final AcademyForm academyForm = new AcademyForm();

		res = this.createFormModelAndView(academyForm);
		return res;
	}

	// (REGISTRO) Guardar en la base de datos el nuevo customer
	@RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid final AcademyForm academyForm, final BindingResult binding) {
		ModelAndView res;
		Academy academy;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(academyForm);
			System.out.println(binding.getAllErrors());
			//Errores no gestionados por binding
			if (!StringUtils.isEmpty(academyForm.getPostalAddress()) && !academyForm.getPostalAddress().matches("^(\\d{5}$)")) {
				res.addObject("postal", "postal");
			}
			if (!StringUtils.isEmpty(academyForm.getPhone()) && !academyForm.getPhone().matches("^((\\+\\d{2})?[ ]?(\\(\\d{1,3}\\))?[ ]?\\d{4,}$)")) {
				res.addObject("phone", "phone");
			}
		} else {
			try {

				academy = academyService.reconstruct(academyForm);

				final List<String> errores = academyService.comprobacionEditarListErrores(academy);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(academyForm);
					for (final String error : errores) {
						res.addObject(error, error);
					}
				} else {
					academyService.saveForm(academy);
					res = new ModelAndView("redirect:/security/login.do");
				}

			} catch (final Throwable oops) {
				res = this.createFormModelAndView(academyForm);
				System.out.println(oops.getLocalizedMessage());
				if (oops.getLocalizedMessage().contains("ConstraintViolationException")) {
					res.addObject("duplicate", "duplicate");
				}
				if (oops.getLocalizedMessage().contains("You must accept the term and conditions")) {
					res.addObject("terms", "terms");
				}
				if (oops.getLocalizedMessage().contains("Passwords do not match")) {
					res.addObject("pass", "pass");
				}
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect")) {
					res.addObject("postal", "postal");
				}
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct")) {
					res.addObject("phone", "phone");
				}
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView para formulario
	protected ModelAndView createFormModelAndView(final AcademyForm academyForm) {
		ModelAndView res;

		res = this.createFormModelAndView(academyForm, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(final AcademyForm academyForm, final String message) {
		ModelAndView res;

		res = new ModelAndView("academy/create");
		res.addObject("academyForm", academyForm);
		res.addObject("message", message);

		return res;
	}

	// (MODIFICAR DATOS) Modificar datos
	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public ModelAndView edit() {
		ModelAndView res;
		Academy academy = academyService.findByPrincipal();

		res = this.createFormModelAndView(academy);
		return res;
	}

	// (MODIFICAR DATOS) Guardar en la base de datos el nuevo academy
	@RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
	public ModelAndView saveEdit(@Valid Academy academy, final BindingResult binding) {
		ModelAndView res;

		if (binding.hasErrors()) {
			res = this.createFormModelAndView(academy);
			System.out.println(binding.getAllErrors());
		} else {
			try {

				final List<String> errores = academyService.comprobacionEditarListErrores(academy);
				if (!CollectionUtils.isEmpty(errores)) {
					res = this.createFormModelAndView(academy);
					for (final String error : errores) {
						res.addObject(error, error);
					}
				} else {
					academyService.save(academy);
					res = new ModelAndView("redirect:/");
				}

			} catch (final Throwable oops) {
				System.out.println(oops);
				res = this.createFormModelAndView(academy);
				if (oops.getLocalizedMessage().contains("The format of the postaladdress is incorrect")) {
					res.addObject("postal", "postal");
				}
				if (oops.getLocalizedMessage().contains("The format of the indicated telephone is not correct")) {
					res.addObject("phone", "phone");
				}
			}
		}

		return res;
	}

	// Creaci�n de ModelAndView para edit
	protected ModelAndView createFormModelAndView(Academy academy) {
		ModelAndView res;

		res = this.createFormModelAndView(academy, null);

		return res;
	}

	protected ModelAndView createFormModelAndView(Academy academy, final String message) {
		ModelAndView res;

		res = new ModelAndView("academy/edit");
		res.addObject("academy", academy);
		res.addObject("message", message);

		return res;
	}

}
