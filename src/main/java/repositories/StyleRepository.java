
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Dancer;
import domain.Style;

@Repository
public interface StyleRepository extends JpaRepository<Style, Integer> {
	
	@Query("select a from Style a order by a.courses.size DESC")
	Collection<Style> stylesOrderByCourses();
	
	@Query("select d from Dancer a join a.applies b join b.course c join c.style d group by d order by count(a) DESC")
	Collection<Style> stylesOrderByDancersWhoCanTeach();
	
}
