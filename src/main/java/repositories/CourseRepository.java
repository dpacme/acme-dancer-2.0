
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Integer> {

	@Query("select min(a.courses.size), max(a.courses.size), avg(a.courses.size), stddev(a.courses.size) from Academy a")
	Collection<Object> minMaxAvgAndStadevOfCoursesPerAcademy();

	@Query("select a from Course a where (a.title like ?1 or a.style.name like ?1 or a.style.description like ?1) and a.academy.id=?2")
	Collection<Course> getCoursesByKeywordAcademy(String keyword, Integer academyId);

	@Query("select a from Course a where (a.title like ?1 or a.style.name like ?1 or a.style.description like ?1) and a.style.id=?2")
	Collection<Course> getCoursesByKeywordStyle(String keyword, Integer styleId);

	@Query("select a from Course a where (a.title like ?1 or a.style.name like ?1 or a.style.description like ?1)")
	Collection<Course> getCoursesByKeyword(String keyword);
}
