
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Apply;

@Repository
public interface ApplyRepository extends JpaRepository<Apply, Integer> {

	@Query("select min(c.applies.size), max(c.applies.size), avg(c.applies.size), stddev(c.applies.size) from Course c")
	Collection<Object> minMaxAvgAndStadevOfAppliesPerCourse();

	@Query("select size(a) from Course c join c.applies a where a.course.id = ?1 and a.dancer.id = ?2")
	Integer existApplyForCourseByDancer(int courseId, int dancerId);

	@Query("select a from Apply a join a.course c where c.academy.id = ?1")
	Collection<Apply> findAppliesFromAcademy(Integer academyId);

}
