
package converters;

import javax.transaction.Transactional;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import domain.Dancer;

@Component
@Transactional
public class DancerToStringConverter implements Converter<Dancer, String> {

	@Override
	public String convert(final Dancer dancer) {
		String res;

		if (dancer == null)
			res = null;
		else
			res = String.valueOf(dancer.getId());

		return res;

	}
}
