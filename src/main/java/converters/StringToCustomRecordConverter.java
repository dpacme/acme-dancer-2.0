
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.CustomRecord;
import repositories.CustomRecordRepository;

@Component
@Transactional
public class StringToCustomRecordConverter implements Converter<String, CustomRecord> {

	@Autowired
	CustomRecordRepository customRecordRepository;


	@Override
	public CustomRecord convert(final String text) {
		CustomRecord res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.customRecordRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}
}
