
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Academy;
import repositories.AcademyRepository;

@Component
@Transactional
public class StringToAcademyConverter implements Converter<String, Academy> {

	@Autowired
	AcademyRepository academyRepository;


	@Override
	public Academy convert(final String text) {
		Academy res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.academyRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}
}
