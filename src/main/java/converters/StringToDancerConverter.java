
package converters;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import domain.Dancer;
import repositories.DancerRepository;

@Component
@Transactional
public class StringToDancerConverter implements Converter<String, Dancer> {

	@Autowired
	DancerRepository dancerRepository;


	@Override
	public Dancer convert(final String text) {
		Dancer res;
		int id;
		try {
			if (StringUtils.isEmpty(text))
				res = null;
			else {
				id = Integer.valueOf(text);
				res = this.dancerRepository.findOne(id);
			}
		} catch (final Throwable oops) {
			throw new IllegalArgumentException(oops);
		}
		return res;
	}
}
