<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<img src="images/logo.png" alt="Acme-Dancer Co., Inc." />
</div>

<div>
	<ul id="jMenu">
		<!-- Do not forget the "fNiv" class for the first level links !! -->
		<security:authorize access="hasRole('ADMINISTRATOR')">
			<li><a class="fNiv" href="administrator/edit.do"><spring:message code="master.page.edit"/></a></li>

			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.styles" /> 
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="style/administrator/create.do"><spring:message code="master.page.style.create" /> </a></li>
					<li><a href="style/list.do"><spring:message code="master.page.style.list" /> </a></li>
				</ul>
			</li>

			<li><a class="fNiv" href="administrator/dashboard.do"><spring:message code="master.page.dashboard" /></a></li>

		</security:authorize>
		
		<security:authorize access="hasRole('DANCER')">
			<li><a class="fNiv" href="dancer/edit.do"><spring:message code="master.page.edit"/></a></li>
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.curricula" /> 
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="curricula/dancer/create.do"><spring:message code="master.page.curricula.create" /> </a></li>
					<li><a href="curricula/dancer/list.do"><spring:message code="master.page.curricula.list" /> </a></li>
				</ul>
			</li>
		</security:authorize>
		
		<security:authorize access="hasRole('ACADEMY')">
			<li><a class="fNiv" href="academy/edit.do"><spring:message code="master.page.edit"/></a></li>
		</security:authorize>
		
		<li><a class="fNiv" href="academy/all.do"><spring:message code="master.page.academies" /></a></li>
		<security:authorize access="!hasRole('ADMINISTRATOR')">
			<li><a href="style/list.do"><spring:message code="master.page.style.listStyles" /> </a></li>
		</security:authorize>
		
		
		<li>
				<a class="fNiv"> 
					<spring:message code="master.page.courses" /> 
				</a>
				<ul>
					<li class="arrow"></li>				
					<li><a href="course/all.do"><spring:message code="master.page.courses.all" /> </a></li>
					<security:authorize access="hasRole('ACADEMY')">						
						<li><a href="course/academy/myCourses.do"><spring:message code="master.page.courses.myCourses" /> </a></li>
					</security:authorize>
					<security:authorize access="hasRole('ACADEMY')">						
						<li><a href="course/academy/create.do"><spring:message code="master.page.courses.create" /></a></li>
					</security:authorize>
					
				</ul>
			</li>		
		
		<security:authorize access="isAnonymous()">
			<li><a class="fNiv" href="security/login.do"><spring:message code="master.page.login" /></a></li>
			<li><a class="fNiv" href="academy/create.do"><spring:message code="master.page.registerAsAcademy"/></a></li>
			<li><a class="fNiv" href="dancer/create.do"><spring:message code="master.page.registerAsDancer"/></a></li>			
		</security:authorize>
		<security:authorize access="hasRole('DANCER')">	
			<li><a class="fNiv" href="apply/dancer/myApplies.do"><spring:message code="master.page.myApplies"/></a></li>			
		</security:authorize>
		<security:authorize access="isAuthenticated()">
			<li>
				<a class="fNiv"> 
					<spring:message code="master.page.profile" /> 
			        (<security:authentication property="principal.username" />)
				</a>
				<ul>
					<li class="arrow"></li>					
					<li><a href="j_spring_security_logout"><spring:message code="master.page.logout" /> </a></li>
				</ul>
			</li>
		</security:authorize>
	</ul>
</div>

<div>
	<a href="?language=en">en</a> | <a href="?language=es">es</a>
</div>

