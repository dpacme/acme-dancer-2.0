<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />

<display:table name="applies" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">
	
	<spring:message code="apply.dancer" var="userHeader" />
	<display:column title="${userHeader}" sortable="true" >
		${row.dancer.name} ${row.dancer.surname}
	</display:column>
	
	<spring:message code="apply.dancer.category" var="categoryHeader" />
	<display:column title="${categoryHeader}" sortable="true" >
		<jstl:choose>
			<jstl:when test="${row.curricula == null}">
				<spring:message code="apply.dancer.category.student"/>
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="apply.dancer.category.teacher"/>
			</jstl:otherwise>
		</jstl:choose>
	</display:column>

	<spring:message code="apply.momment" var="mommentHeader" />
	<display:column property="momment" format="{0,date,dd/MM/YYYY}" title="${mommentHeader}" sortable="true" />

	<spring:message code="apply.status" var="statusHeader" />
	<display:column title="${statusHeader}" sortable="true">
		<jstl:if test="${row.status == 'accepted'}">
			<spring:message code="apply.status.accepted"/>
		</jstl:if>
		<jstl:if test="${row.status == 'rejected'}">
			<spring:message code="apply.status.rejected"/>
		</jstl:if>
		<jstl:if test="${row.status == 'pending'}">
			<spring:message code="apply.status.pending"/>
		</jstl:if>
	</display:column>

	<security:authorize access="hasRole('ACADEMY')">
		<spring:message code="apply.course" var="courseHeader" />
		<display:column title="${courseHeader}">
			<acme:button href="course/showDisplay.do?courseId=${row.course.id}"
				name="seeCourse" code="apply.course.see" />
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('DANCER')">
		<spring:message code="apply.course" var="courseHeader" />
		<display:column title="${courseHeader}">
			<acme:button href="course/showDisplay.do?courseId=${row.course.id}"
				name="seeCourse" code="apply.course.see" />
		</display:column>
	</security:authorize>
	
	<security:authorize access="hasRole('ACADEMY')">
	<spring:message code="apply.actions" var="actionsHeader" />
	<jstl:if test="${row.course.academy.userAccount.id == loginService.getPrincipal().getId()}">
		<display:column title="${actionsHeader}">				
				<jstl:choose>
					<jstl:when test="${row.status != 'accepted' && row.status != 'Accepted'}">
						<jstl:choose>
							<jstl:when test="${( (row.course.status == 'organising' || row.course.status == 'Organising') && row.curricula != null) 
							|| ((row.course.status == 'delivering' || row.course.status == 'Delivering') && row.curricula == null)}">
								<div class="inline">
									<acme:button href="apply/academy/setApplyAsAccepted.do?applyID=${row.id}"
									name="accepted" code="apply.setAsAccepted" />
								</div>
							</jstl:when>
							<jstl:otherwise>
								<div class="inline">
									<input type="button" href="#"
									disabled="disabled"
									name="accepted" value='<spring:message code="apply.setAsAccepted" />' />
								</div>
							</jstl:otherwise>
						</jstl:choose>						
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" href="#"
							disabled="disabled"
							name="accepted" value='<spring:message code="apply.setAsAccepted" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>
				<jstl:choose>
					<jstl:when test="${row.status != 'rejected' && row.status != 'Rejected'}">
						<div class="inline">
							<acme:button href="apply/academy/setApplyAsRejected.do?applyID=${row.id}"
							name="rejected" code="apply.setAsRejected" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" href="#"
							disabled="disabled"
							name="rejected" value='<spring:message code="apply.setAsRejected" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>		
				<jstl:choose>
					<jstl:when test="${row.status != 'pending'  && row.status != 'Pending'}">
						<div class="inline">
							<acme:button href="apply/academy/setApplyAsPending.do?applyID=${row.id}"
							name="pending" code="apply.setAsPending" />
						</div>
					</jstl:when>
					<jstl:otherwise>
						<div class="inline">
							<input type="button" href="#"
							disabled="disabled"
							name="pending" value='<spring:message code="apply.setAsPending" />' />
						</div>
					</jstl:otherwise>
				</jstl:choose>
			</display:column>
	</jstl:if>
	</security:authorize>

</display:table>

<security:authorize access="hasRole('ACADEMY')">
	<jstl:if test="${row.course.academy.userAccount.id == loginService.getPrincipal().getId()}">
		<jstl:choose>
				<jstl:when test="${course.status == 'organising' || course.status == 'Organising'}">
					<spring:message code="course.status.organising.apply.info" />
				</jstl:when>
				<jstl:when test="${course.status == 'delivering' || course.status == 'Delivering'}">
					<spring:message code="course.status.delivering.apply.info" />
				</jstl:when>				
			</jstl:choose>
	</jstl:if>
</security:authorize>

