<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<div>
	<ul>
		<li><b><spring:message code="academy.comercialName" />:</b>
			${academy.comercialName}</li>
		<li><b><spring:message code="academy.name" />:</b>
			${academy.name}</li>
		<li><b><spring:message code="academy.surname" />:</b>
			${academy.surname}</li>
		<li><b><spring:message code="academy.email" />:</b>
			${academy.email}</li>
		<li><b><spring:message code="academy.phone" />:</b>
			${academy.phone}</li>
		<li><b><spring:message code="academy.postalAddress" />:</b>
			${academy.postalAddress}</li>
	</ul>
</div>

<h2><spring:message code="academy.courses"/> </h2>

<display:table name="courses" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="academy.title" var="titleNameHeader" />
	<display:column property="title" title="${titleNameHeader}"
		sortable="true" />

	<spring:message code="academy.details" var="detailsHeader" />
	<display:column title="${detailsHeader}">
		<acme:button href="course/showDisplay.do?courseId=${row.id}"
			name="see" code="academy.see" />
	</display:column>


</display:table>

<br />
