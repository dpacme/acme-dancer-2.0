<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<display:table name="academies" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="academy.comercialName" var="comercialNameHeader" />
	<display:column property="comercialName" title="${comercialNameHeader}"
		sortable="true" />

	<spring:message code="academy.details" var="detailsHeader" />
	<display:column title="${detailsHeader}">
		<acme:button href="academy/showDisplay.do?academyId=${row.id}"
			name="see" code="academy.see" />
	</display:column>

	<spring:message code="academy.courses" var="coursesHeader" />
	<display:column title="${coursesHeader}">
		<acme:button href="course/coursesByAcademy.do?academyId=${row.id}"
			name="see" code="academy.see" />
	</display:column>


</display:table>



