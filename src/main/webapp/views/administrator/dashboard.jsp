<%--
 * action-1.jsp
 *
 * Copyright (C) 2016 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<security:authorize access="hasRole('ADMINISTRATOR')">

	<h2>
		<spring:message code="administrator.minMaxAvgAndStadevOfAppliesPerCourse" />
	</h2>
	<jstl:out value="${minAppliesPerCourse}" />
	<br>
	<jstl:out value="${maxAppliesPerCourse}" />
	<br>
	<jstl:out value="${avgAppliesPerCourse}" />
	<br>
	<jstl:out value="${stdevAppliesPerCourse}" />
	<br>

	<h2>
		<spring:message code="administrator.minMaxAvgAndStadevOfCoursesPerAcademy" />
	</h2>
	
	<jstl:out value="${minCoursesPerAcademy}" />
	<br>
	<jstl:out value="${maxCoursesPerAcademy}" />
	<br>
	<jstl:out value="${avgCoursesPerAcademy}" />
	<br>
	<jstl:out value="${stdevCoursesPerAcademy}" />
	<br>
	
	<h2>
		<spring:message code="administrator.ratioDancersWithCurriculum" />
	</h2>
	
	<jstl:out value="${ratioDancersWithCurriculum}" />
	<br>
	
	<h2>
		<spring:message code="administrator.stylesOrderByCourses" />
	</h2>
	
	<display:table name="stylesOrderByCourses" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

		<spring:message code="style.name" var="nameHeader" />
		<display:column property="name" title="${nameHeader}" sortable="true" />
		
		<spring:message code="style.description" var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}" sortable="true" />
		
		<spring:message code="style.pictures" var="pictureHeader" />
		<display:column title="${pictureHeader}">
			<jstl:forEach var="picture" items="${row.pictures}">
				<a href="${picture}" target="_blank"><img src="${picture}"
					height="64" width="64"></a>
			</jstl:forEach>
		</display:column>
		
		
		<spring:message code="style.videos" var="videoHeader" />
		<display:column title="${videoHeader}">
			<jstl:forEach var="video" items="${row.videos}">
				<a href="${video.toString()}" target="_blank">${video}</a>
				<br/>
			</jstl:forEach>
		</display:column>
		
		<spring:message code="style.courses" var="courseHeader" />
		<display:column title="${courseHeader}">
			<acme:button href="course/listByStyle.do?styleId=${row.id}"
				name="seeCourse" code="style.course.see" />
		</display:column>
		
		<security:authorize access="hasRole('ADMINISTRATOR')">
			<display:column>
				<acme:button href="style/administrator/edit.do?styleId=${row.id}"
					name="editStyle" code="style.course.edit" />
			</display:column>
		</security:authorize>
	
	</display:table>
	
	<h2>
		<spring:message code="administrator.stylesOrderByDancersWhoCanTeach" />
	</h2>
	
	<display:table name="stylesOrderByDancersWhoCanTeach" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

		<spring:message code="style.name" var="nameHeader" />
		<display:column property="name" title="${nameHeader}" sortable="true" />
		
		<spring:message code="style.description" var="descriptionHeader" />
		<display:column property="description" title="${descriptionHeader}" sortable="true" />
		
		<spring:message code="style.pictures" var="pictureHeader" />
		<display:column title="${pictureHeader}">
			<jstl:forEach var="picture" items="${row.pictures}">
				<a href="${picture}" target="_blank"><img src="${picture}"
					height="64" width="64"></a>
			</jstl:forEach>
		</display:column>
		
		
		<spring:message code="style.videos" var="videoHeader" />
		<display:column title="${videoHeader}">
			<jstl:forEach var="video" items="${row.videos}">
				<a href="${video.toString()}" target="_blank">${video}</a>
				<br/>
			</jstl:forEach>
		</display:column>
		
		<spring:message code="style.courses" var="courseHeader" />
		<display:column title="${courseHeader}">
			<acme:button href="course/listByStyle.do?styleId=${row.id}"
				name="seeCourse" code="style.course.see" />
		</display:column>
		
		<security:authorize access="hasRole('ADMINISTRATOR')">
			<display:column>
				<acme:button href="style/administrator/edit.do?styleId=${row.id}"
					name="editStyle" code="style.course.edit" />
			</display:column>
		</security:authorize>
	
	</display:table>
	
</security:authorize>