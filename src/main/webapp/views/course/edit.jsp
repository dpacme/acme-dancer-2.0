<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<script>
	$(function() {

		$('.timepicker').timepicker();

		$('.datetimepicker').datepicker({
			dateFormat : 'dd/mm/yy',
		});

	});
</script>


<form:form action="${requestURI}" modelAttribute="course">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="applies" />
	<form:hidden path="academy" />

	<jstl:if test="${requestURI.contains('create')}">
		<form:hidden path="status" />
	</jstl:if>


	<acme:textbox code="course.title" path="title" />

	<form:label path="level">
		<spring:message code="course.level" />
	</form:label>
	<b></b>
	<form:select path="level" style="margin-bottom: 10px">
		<form:option value="Beginner">
			<spring:message code="course.beginner" />
		</form:option>
		<form:option value="Intermediate">
			<spring:message code="course.intermediate" />
		</form:option>
		<form:option value="Advanced">
			<spring:message code="course.advanced" />
		</form:option>
		<form:option value="Professional">
			<spring:message code="course.professional" />
		</form:option>
	</form:select>
	<form:errors class="error" path="dayWeek" />
	<br />

	<spring:message code="course.date.click" var="datePlaceholder" />
	<spring:message code="course.starDate" />:
	<form:input type="text" id="startDate" path="startDate"
		readonly="readonly" placeholder="${datePlaceholder}"
		class="datetimepicker" />
	<form:errors class="error" path="startDate" />
	<br />

	<spring:message code="course.endDate" />:
	<form:input type="text" id="endDate" path="endDate" readonly="readonly"
		placeholder="${datePlaceholder}" class="datetimepicker" />
	<form:errors class="error" path="endDate" />
	<jstl:if test="${errorDate!=null}">
		<span class="message"><spring:message code="${errorDate}" /></span>
	</jstl:if>
	<br />

	<form:label path="dayWeek">
		<spring:message code="course.dayWeek" />
	</form:label>
	<b></b>
	<form:select path="dayWeek" style="margin-bottom: 10px">
		<form:option value="Monday">
			<spring:message code="course.dayWeekMonday" />
		</form:option>
		<form:option value="Tuesday">
			<spring:message code="course.dayWeekTuesday" />
		</form:option>
		<form:option value="Wednesday">
			<spring:message code="course.dayWeekWednesday" />
		</form:option>
		<form:option value="Thursday">
			<spring:message code="course.dayWeekThursday" />
		</form:option>
		<form:option value="Friday">
			<spring:message code="course.dayWeekFriday" />
		</form:option>
		<form:option value="Saturday">
			<spring:message code="course.dayWeekSaturday" />
		</form:option>
		<form:option value="Sunday">
			<spring:message code="course.dayWeekSunday" />
		</form:option>
	</form:select>
	<form:errors class="error" path="dayWeek" />
	<br />

	<spring:message code="course.time.click" var="timePlaceholder" />
	<spring:message code="course.time" />:
	<form:input type="text" id="time" path="time" readonly="readonly"
		placeholder="${timePlaceholder}" class="timepicker" />
	<form:errors class="error" path="time" />

	<br />

	<form:label path="style">
		<spring:message code="course.style" />
	</form:label>
	<form:select id="style" path="style">
		<jstl:forEach var="r" items="${styles}">
			<form:option label="${r.name}" value="${r.id}" />
		</jstl:forEach>
	</form:select>
	<jstl:if test="${styleError != null}">
		<span class="message"><spring:message code="${styleError}" /></span>
	</jstl:if>
	<form:errors path="style" cssClass="error" />
	<br />
	<jstl:if test="${requestURI.contains('edit')}">
		<form:label path="status">
	        <spring:message code="course.status" />
	    </form:label><b></b>
		<form:select path="status" style="margin-bottom: 10px">
			<form:option value="Organising"><spring:message code="course.status.organising"/></form:option>
			<form:option value="Delivering"><spring:message code="course.status.delivering"/></form:option>
		</form:select>
		<form:errors class="error" path="status" />
		<br/>
		
	</jstl:if>
	<br />
	

	<!-- Acciones -->
	<acme:submit name="save" code="course.save" />

	<jstl:if test="${requestURI.contains('edit')}">
		<acme:submit name="delete" code="course.delete" />
	</jstl:if>

	<acme:cancel url="course/academy/myCourses.do" code="course.cancel" />

</form:form>

<br>

