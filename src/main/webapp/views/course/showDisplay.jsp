<%--
 * index.jsp
 *
 * Copyright (C) 2014 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div>
	<ul>
		<li><b><spring:message code="course.title" />:</b>
			${course.title}</li>
		<li><b><spring:message code="course.level" />:</b>
			${course.level}</li>
		<li><b><spring:message code="course.starDate" />:</b> <fmt:formatDate
				value="${starDate}" pattern="dd/MM/YYYY" /></li>
		<li><b><spring:message code="course.endDate" />:</b> <fmt:formatDate
				value="${endDate}" pattern="dd/MM/YYYY" /></li>
		<li><b><spring:message code="course.dayWeek" />:</b>
			${course.dayWeek}</li>
		<li><b><spring:message code="course.time" />:</b> <fmt:formatDate
				value="${time}" pattern="HH:mm" /></li>
		</li>
		<li><b><spring:message code="course.status" />:</b> <jstl:choose>
				<jstl:when
					test="${course.status == 'organising' || course.status == 'Organising'}">
					<spring:message code="course.status.organising" />
				</jstl:when>
				<jstl:when
					test="${course.status == 'delivering' || course.status == 'Delivering'}">
					<spring:message code="course.status.delivering" />
				</jstl:when>
			</jstl:choose></li>
	</ul>
</div>
<security:authorize access="hasRole('DANCER')">

	<fieldset style="width: 20%">
		<legend>
			<b><spring:message code="course.apply" /></b>
		</legend>
		<jstl:choose>
			<jstl:when test="${isTeacher || isStudent}">
				<jstl:if test="${isTeacher && isStudent}">
					<h3>
						<spring:message code="course.applyTeacher" />
					</h3>
					<jstl:choose>
						<jstl:when test="${!haveCurricula}">

							<spring:message code="course.applyCurricula" />

						</jstl:when>
						<jstl:otherwise>
							<spring:message code="course.selectCurricula" />
							<form action="apply/dancer/applyForTeacher.do?" method="post">
								<input type="hidden" name="offerId" value="${course.id}" /> <select
									name="curriculaId" style="margin-bottom: 10px">
									<jstl:forEach var="curricula" items="${allCurriculas}">
										<option label="${curricula.ticker}" value="${curricula.id}">
										${curricula.ticker}
										</option>
									</jstl:forEach>
								</select> <input type="submit"
									value="<spring:message code="course.applyTeacher" />" />
							</form>
						</jstl:otherwise>
					</jstl:choose>
				</jstl:if>
				<jstl:if test="${isStudent}">
					<br />
					<h3>
						<spring:message code="course.applyStudent" />
					</h3>
					<acme:button
						href="apply/dancer/applyForStudent.do?courseId=${course.id}"
						name="applyCourse" code="course.applyStudent" />
				</jstl:if>
			</jstl:when>
			<jstl:otherwise>
				<spring:message code="course.applyRealized" />
			</jstl:otherwise>
		</jstl:choose>
	</fieldset>
</security:authorize>


<br />
