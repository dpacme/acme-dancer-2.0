<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<fieldset style="width: 20%">
	<legend>
		<spring:message code="course.search" />
	</legend>
	<form method="POST" action="course/search.do">

		<spring:message code="course.keyword" />
		<spring:message code="course.keyword" var="keywordPlaceholder" />
		<input type="text" id="keyword" name="keyword"
			placeholder="${keywordPlaceholder}" /> <br /> <input type="hidden"
			id="academyId" name="academyId" value="${academyId}"
			readonly="readonly" /> <input type="hidden" id="styleId"
			name="styleId" value="${styleId}" readonly="readonly" /> <input
			type="submit" name="search"
			value="<spring:message code="course.search"/>" />
	</form>
</fieldset>

<h2 style="color: blue;"><spring:message code="course.canApply" /></h2>	

<display:table name="courses" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="course.title" var="titleHeader" />
	<display:column property="title" title="${titleHeader}" sortable="true" />

	<spring:message code="course.style.name" var="nameHeader" />
	<display:column property="style.name" title="${nameHeader}"
		sortable="true" />

	<spring:message code="course.style.description" var="descriptionHeader" />

	<display:column property="style.description" title="${descriptionHeader}" sortable="true" />
	
	<spring:message code="course.status" var="statusHeader" />
	<display:column title="${statusHeader}" sortable="true" >
		<jstl:if test="${row.status == 'organising' || row.status == 'Organising'}">
			<spring:message code="course.status.organising" />
		</jstl:if>
		<jstl:if
			test="${row.status == 'delivering' || row.status == 'Delivering'}">
			<spring:message code="course.status.delivering" />
		</jstl:if>
	</display:column>
	

	<spring:message code="course.style.detail" var="detailsStyleHeader" />
	<display:column title="${detailsStyleHeader}">
		<acme:button href="style/showDisplay.do?courseId=${row.id}" name="see"
			code="course.see" />
	</display:column>

	<spring:message code="course.details" var="detailsHeader" />
	<display:column title="${detailsHeader}">
		<acme:button href="course/showDisplay.do?courseId=${row.id}"
			name="see" code="course.see" />
	</display:column>

	<spring:message code="course.academy" var="academyHeader" />
	<display:column title="${academyHeader}">
		<acme:button href="academy/showDisplay.do?academyId=${row.academy.id}"
			name="see" code="course.see" />
	</display:column>

	<security:authorize access="hasRole('ACADEMY')">
		<spring:message code="course.applies" var="appliesHeader" />
		<display:column title="${appliesHeader}">
			<jstl:if
				test="${row.academy.userAccount.id == loginService.getPrincipal().getId()}">
				<acme:button
					href="apply/academy/appliesFromCourse.do?courseId=${row.id}"
					name="seeApplies" code="course.applies.see" />
			</jstl:if>
		</display:column>
	</security:authorize>


	<security:authorize access="hasRole('ACADEMY')">
		<display:column>
			<jstl:if
				test="${row.academy.userAccount.id == loginService.getPrincipal().getId()}">
				<acme:button href="course/academy/edit.do?courseId=${row.id}"
					name="editCourse" code="course.edit" />
			</jstl:if>
		</display:column>
	</security:authorize>



</display:table>


<jstl:if test="${requestURI.contains('myCourses')}">
	<a href="course/academy/create.do"><spring:message
			code="course.create" /></a>
</jstl:if>

