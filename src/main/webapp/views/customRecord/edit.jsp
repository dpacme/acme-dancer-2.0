<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="customRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curricula" />

	<acme:textbox code="curricula.customRecord.title" path="title" />
	<acme:textbox code="curricula.customRecord.description" path="description" />
	
	<form:label path="links">
        <spring:message code="curricula.customRecord.links" />
    </form:label><b></b>
    <form:input type="url" path="links" />
    <form:errors class="error" path="links" />	
	<br>
	
	<br>
	<b><spring:message code="customRecord.linksW" /></b>
	<br>
	<br>
	
	<input type="submit" name="save"
		value="<spring:message code="styleRecord.save" />" />&nbsp;
	
	<jstl:if test="${requestURI.contains('edit')}">
		<input type="submit" name="delete"
			value="<spring:message code="styleRecord.delete" />" />&nbsp; 
	</jstl:if>
		
	<input type="button" name="cancel"
		value="<spring:message code="curricula.cancel" />"
		onclick="javascript: window.location.replace('curricula/dancer/list.do');" /> <br />
		
		

</form:form>

