<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
				scope="page" />

<display:table name="styles" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="style.name" var="nameHeader" />
	<display:column property="name" title="${nameHeader}" sortable="true" />
	
	<spring:message code="style.description" var="descriptionHeader" />
	<display:column property="description" title="${descriptionHeader}" sortable="true" />
	
	<spring:message code="style.pictures" var="pictureHeader" />
	<display:column title="${pictureHeader}">
		<jstl:forEach var="picture" items="${row.pictures}">
			<a href="${picture}" target="_blank"><img src="${picture}"
				height="64" width="64"></a>
		</jstl:forEach>
	</display:column>
	
	
	<spring:message code="style.videos" var="videoHeader" />
	<display:column title="${videoHeader}">
		<jstl:forEach var="video" items="${row.videos}">
			<a href="${video.toString()}" target="_blank">${video}</a>
			<br/>
		</jstl:forEach>
	</display:column>
	
	<spring:message code="style.courses" var="courseHeader" />
	<display:column title="${courseHeader}">
		<acme:button href="course/listByStyle.do?styleId=${row.id}"
			name="seeCourse" code="style.course.see" />
	</display:column>
	
	<security:authorize access="hasRole('ADMINISTRATOR')">
		<display:column>
			<acme:button href="style/administrator/edit.do?styleId=${row.id}"
				name="editStyle" code="style.course.edit" />
		</display:column>
	</security:authorize>

</display:table>

<security:authorize access="hasRole('ADMINISTRATOR')">
	<jstl:if test="${requestURI.contains('list.do')}">
		<a href="style/administrator/create.do"><spring:message code="style.create" /></a>
	</jstl:if>
</security:authorize>	



