<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form action="${requestURI}" modelAttribute="styleRecord">

	<form:hidden path="id" />
	<form:hidden path="version" />
	<form:hidden path="curricula" />

	<spring:message code="curricula.styleRecord.years" />
	
	<form:input type="number" min="0" step="1" path="years"/>
	<form:errors cssClass="error" path="years" />
	<div style="overflow: hidden">
			<div class="inline">
				<acme:select items="${styles}" itemLabel="name" code="curricula.styleRecord.style" path="style"/>
			</div>
			<jstl:if test="${style != null}">
				<div>
					<span class="message"><spring:message code="${style}" /></span>
				</div>
			</jstl:if>
		</div>
		<br />
	
	<br>

	<jstl:if test="${requestURI.contains('create')}">
		<input type="submit" name="next"
			value="<spring:message code="curricula.moreStyleRecords" />" />&nbsp; 
	</jstl:if>
		
	<input type="submit" name="save"
		value="<spring:message code="styleRecord.save" />" />&nbsp;
	
	<jstl:if test="${requestURI.contains('edit')}">
		<jstl:if test="${styleRecord.getCurricula().getStyleRecords().size()>1}">
			<input type="submit" name="delete"
				value="<spring:message code="styleRecord.delete" />" />&nbsp; 
		</jstl:if>
	</jstl:if>
		
	<input type="button" name="cancel"
		value="<spring:message code="curricula.cancel" />"
		onclick="javascript: window.location.replace('curricula/dancer/list.do');" /> <br />
		
</form:form>

<jstl:if test="${sizeStyleRecords!=null}">
	<span class="message"><spring:message code="${sizeStyleRecords}" /></span>
</jstl:if>

