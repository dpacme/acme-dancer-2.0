<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />
<jstl:if test="${curricula.getDancer().getUserAccount().getId()==loginService.getPrincipal().getId()}">
	<acme:button href="curricula/dancer/delete.do?curriculaId=${curricula.getId()}"
					name="delete" code="curricula.deleteMyCurricula" />
</jstl:if>

<div class="personalRecord" style="text-align: center">
	<br/>
	<b><spring:message code="curricula.personal" /></b>
	<p />

	<b><spring:message code="curricula.personal.fullName" />:</b> ${personalRecord.fullName}<br>
	<p />
	<b><spring:message code="curricula.personal.email" />:</b> ${personalRecord.email}<br>
	<p />
	<b><spring:message code="curricula.personal.whatsapp" />:</b> ${personalRecord.whatsapp}<br>
	<p />
	<b><spring:message code="curricula.personal.linkedInUrl" />:</b> <a
		href="${personalRecord.linkedInUrl}"> ${personalRecord.linkedInUrl}</a><br>
	<p />
	
	<acme:button href="personalRecord/dancer/edit.do?curriculaId=${curricula.getId()}"
				name="edit" code="curricula.edit" />
</div>

<br/>
<b><spring:message code="curricula.styleRecords" /></b>
<p />

<display:table name="styleRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.styleRecord.years" var="yearsHeader" />
	<display:column title="${yearsHeader}" property="years" sortable="true"/>
	
	<spring:message code="curricula.styleRecord.style" var="styleHeader" />
	<display:column title="${styleHeader}">
			<acme:button href="style/show.do?styleRecordId=${row.id}"
				name="edit" code="curricula.see" />
	</display:column>
	
	<display:column>
			<acme:button href="styleRecord/dancer/edit.do?styleRecordId=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="styleRecord/dancer/create.do?curriculaId=${curricula.id}"><spring:message code="record.create"/></a>

<br/>
<br/>
<b><spring:message code="curricula.customRecords" /></b>
<p />

<display:table name="customRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.customRecord.title" var="titleHeader" />
	<display:column title="${titleHeader}" property="title" sortable="true"/>
	
	<spring:message code="curricula.customRecord.description" var="descriptionHeader" />
	<display:column title="${descriptionHeader}" property="description" sortable="true"/>
		
	<spring:message code="curricula.customRecord.links" var="linksHeader" />
	<display:column title="${linksHeader}">
		<jstl:forEach var="link" items="${row.links}">
			<a href="${link.toString()}" target="_blank">${link}</a>
			<br/>
		</jstl:forEach>
	</display:column>
	
	<display:column>
			<acme:button href="customRecord/dancer/edit.do?customRecordId=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="customRecord/dancer/create.do?curriculaId=${curricula.id}"><spring:message code="record.create"/></a>

<br/>
<br/>
<b><spring:message code="curricula.endorserRecords" /></b>
<p />

<display:table name="endorserRecords" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">


	<spring:message code="curricula.endorserRecord.fullName" var="fullNameHeader" />
	<display:column title="${fullNameHeader}" property="fullName" sortable="true"/>
	
	<spring:message code="curricula.endorserRecord.contactMeans" var="contactMeansHeader" />
	<display:column title="${contactMeansHeader}">
		<jstl:forEach var="contactMean" items="${row.contactMeans}">
			${contactMean}<br/>
		</jstl:forEach>
	</display:column>
	
	<display:column>
			<acme:button href="endorserRecord/dancer/edit.do?endorserRecordId=${row.id}"
				name="edit" code="curricula.edit" />
	</display:column>

</display:table>

<br/>
<br/>
<a href="endorserRecord/dancer/create.do?curriculaId=${curricula.id}"><spring:message code="record.create"/></a>
	
