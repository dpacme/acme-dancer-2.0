<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<jsp:useBean id="loginService" class="security.LoginService"
	scope="page" />

<display:table name="curriculas" id="row" requestURI="${requestURI}"
	pagesize="5" class="displaytag">

	<spring:message code="curricula.ticker" var="tickerHeader" />
	<display:column property="ticker" title="${tickerHeader}" sortable="true" />

	<display:column>
		<acme:button href="curricula/dancer/show.do?curriculaId=${row.id}"
			name="show" code="curricula.show" />
	</display:column>
	
	<jstl:if test="${row.getDancer().getUserAccount().getId()==loginService.getPrincipal().getId()}">
		<display:column>
			<acme:button href="curricula/dancer/delete.do?curriculaId=${row.id}"
				name="delete" code="curricula.delete" />
		</display:column>
	</jstl:if>
</display:table>

<a href="curricula/dancer/create.do"><spring:message code="curricula.create"/></a>

