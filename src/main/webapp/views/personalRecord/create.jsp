<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<!-- Edici�n de artist (como usuario autentificado) -->

<jstl:if test="${existeCurricula != null}">
	<div>
		<span class="message"><spring:message code="${existeCurricula}" /></span>
	</div>
</jstl:if>

<jstl:if test="${existeCurricula == null}">
	<form:form action="${requestURI}" modelAttribute="curriculaForm">

		<b><spring:message code="curricula.personal" /></b>

		<br />

		<acme:textbox code="curricula.personal.fullName" path="fullName" />
		<acme:textbox code="curricula.personal.email" path="email" />
		<div style="overflow: hidden">
			<div class="inline">
				<acme:textbox code="curricula.personal.whatsapp" path="whatsapp"
					placeholder="+XX (YYY) ZZZZ" />
			</div>
			<jstl:if test="${whatsappEmail != null}">
				<div>
					<span class="message"><spring:message
							code="${whatsappEmail}" /></span>
				</div>
			</jstl:if>
			<jstl:if test="${whatsapp != null}">
				<div>
					<span class="message"><spring:message code="${whatsapp}" /></span>
				</div>
			</jstl:if>
		</div>
		<br />
		<acme:textbox code="curricula.personal.linkedInUrl" path="linkedInUrl" />
		<br>

		<b><spring:message code="curricula.firstStyleRecord" /></b>

		<br />

		<spring:message code="curricula.styleRecord.years" />
		
		<form:input type="number" min="0" step="1" path="years"/>
		
		<form:errors cssClass="error" path="years" />
		<br />
		<acme:select items="${styles}" itemLabel="name"
			code="curricula.styleRecord.style" path="style" />
		<br>

		<input type="submit" name="next"
			value="<spring:message code="curricula.moreStyleRecords" />" />&nbsp; 
			
		<input type="submit" name="save"
			value="<spring:message code="curricula.save" />" />&nbsp;
			
		<input type="button" name="cancel"
			value="<spring:message code="curricula.cancel" />"
			onclick="javascript: window.location.replace('curricula/dancer/list.do');" />
		<br />

	</form:form>

	<br>
</jstl:if>


